
// 后退操作

function go_back(){
	
	var current_location = window.location.href;
	var prev_location = document.referrer;
	var next_location = '';
	
	if(current_location.match('index') && !prev_location.match('member')){
		window.location.href = APPMAIN;
		return false;
	}
	if(current_location.match('courseinfo') && prev_location.match('login')){
		window.location.href = APPMAIN + '/course/index';
		return false;
	}
	if(current_location.match('courseinfo') && prev_location.match('courseinfo')){
		window.location.href = APPMAIN + '/course/index';
		return false;
	}
	if(current_location.match('packageinfo') && prev_location.match('login')){
		window.location.href = APPMAIN + '/package/index';
		return false;
	}
	if(current_location.match('personaldata')){
		window.location.href = APPMAIN + '/member/index';
		return false;
	}
	if(current_location.match('myorder')){
		window.location.href = APPMAIN + '/member/index';
		return false;
	}
	
	window.location.href = window.history.go(-1);
	
}

// 获取文档高度
function getDocumentTop(){

	var scrollTop = 0, bodyScrollTop = 0, documentScrollTop = 0;
	if(document.body){
		bodyScrollTop = document.body.scrollTop;
	}
	if(document.documentElement){
		documentScrollTop = document.documentElement.scrollTop;
	}

	scrollTop = (bodyScrollTop > documentScrollTop)? bodyScrollTop : documentScrollTop;

	return scrollTop;
}


// 获取可视窗口高度
function getWindowHeight(){

	var windowHeight = 0;

	if(document.compatMode == 'CSS1Compat'){
		windowHeight = document.documentElement.clientHeight;
	}else{
		windowHeight = document.body.clientHeight;
	}

	return windowHeight;
}

// 获取滚动条高度
function getScrollHeight(){

	var scrollHeight = 0, bodyScrollHeight = 0, documentScrollHeight = 0;

	if(document.body){
		bodyScrollHeight = document.body.scrollHeight;
	}
	if(document.documentElement){
		documentScrollHeight = document.documentElement.scrollHeight;
	}

	scrollHeight = (bodyScrollHeight > documentScrollHeight)? bodyScrollHeight : documentScrollHeight;

	return scrollHeight;
}



