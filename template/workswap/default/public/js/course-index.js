
/*改变排序*/
function alter_order(element){

	var status = $(element).find("span").html();
	
	if(status == "↓"){
		
		$(element).find("span").html("↑");
		sort = "asc";
	}else{
		$(element).find("span").html("↓");
		sort = "desc";
	}
	
	order = $(element).attr("order");
	
	page = 1;
	is_final = 0;
	loadInfo1();
}

/*改变专业*/
function alter_subject(element){

	$(element).siblings().find(".child-list").hide();
	$(element).find(".child-list").toggle();
}

/*改变搜索框的定位方式*/
$(document).ready(function(){

	var search_height = $(".float-search").offset().top;
	var compare_height = search_height + 40;
	
	$(window).scroll(function(){

		var scroll_height = $(window).scrollTop();
		
		if(scroll_height <= compare_height){

			$(".float-search").css("position","relative");
			$(".float-search").css("top","0px");
		}else{
			$(".float-search").css("position","fixed");
			$(".float-search").css("top","40px");
		}
	});
})







