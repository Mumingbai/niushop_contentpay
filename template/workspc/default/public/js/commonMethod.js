
	/**
	 * 信息提示
	 * 
	 * @param message
	 *            提示消息
	 * @param options
	 *            提示消息
	 * @param end
	 *            无论是确认还是取消，只要层被销毁了，end都会执行，不携带任何参数。
	 */
	$.msg = function(content, options, end) {
		
		if (layer) {

			if ($.isFunction(options)) {
				end = options;
				options = {};
			}

			options = $.extend({
				time: 2000
			}, options);
			layer.msg(content, options, function() {
				if ($.isFunction(end)) {
					end.call(layer);
				}
			});

		} else {
			alert("缺少组件：" + content);
		}
	};
