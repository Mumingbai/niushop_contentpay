# Niushop知识付费系统-牛小课
![输入图片说明](https://images.gitee.com/uploads/images/2020/1212/141721_892d857e_6569472.png "QQ图片20201212101534.png")
#### 介绍
牛小课商城系统（知识付费商城系统）源码百分百开源，支持永久免费商用，支持形式多样的在线课程图文、视频和录播课程等，以适应各种行业需求，可有效提升课程内容质量。完善的知识商品变现方案，覆盖多种内容服务变现形式，打通知识付费全核心环节，轻松构建业务闭环，促进商业转化。

#### 课程特色

1、管理端首页可对整体运营状况统一管理，很直观的体现待处理的订单数，哪些类型的课程销售量居高，每日新增订单数，新增的关注数等，商家可随时根据数据调整运营方案

![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/155200_92e12fbe_6569472.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/155454_45f0a5cd_6569472.png "屏幕截图.png")

2、课程管理--课程可以根据章节进行管理销售，也可以根据套餐进行管理销售

![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/104333_a8e35cf4_6569472.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/112047_42978009_6569472.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/113249_e63c4171_6569472.png "屏幕截图.png")

3、前台会员购买课程可以直接购买，也可以根据章节单独购买某一章节
![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/123228_5141463d_6569472.png "屏幕截图.png")

4、可以设置该课程是否试听，或者该课程某一章节是否试听，在试听满意后可以购买线上课程学习
![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/144241_b75fc220_6569472.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/123416_24c3d3f3_6569472.png "屏幕截图.png")

5、文章管理模块可添加最新的新闻咨询，在第一时间让用户了解最新消息动态

![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/143655_fd9879d8_6569472.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/143901_02bb80ed_6569472.png "屏幕截图.png")

6、可以针对网站具体模块的菜单单独进行设置，强化客户引导性。

![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/151223_c574a8eb_6569472.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/151245_06940ca1_6569472.png "屏幕截图.png")

7、可在首页单独编辑广告位

![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/151959_94807c11_6569472.png "屏幕截图.png")

8、首页模块可以在后台管理端自行设置板块名称

![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/152226_d65efb9a_6569472.png "屏幕截图.png")

9、设置好板块之后，就可以在网站--首页楼层中自主添加该板块

![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/152503_a62d73f8_6569472.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/152536_9e459abe_6569472.png "屏幕截图.png")

10、支持在线支付（微信、支付宝）与线下支付
![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/153341_a73f9083_6569472.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/152706_96ec10a8_6569472.png "屏幕截图.png")

11、消息通知有邮件消息通知和短信通知

![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/153605_10369c43_6569472.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/153626_65d1b4a3_6569472.png "屏幕截图.png")

12、可以对系统模块自行选择显示隐藏

![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/154009_924e7aab_6569472.png "屏幕截图.png")

13、会员中心可以直观的体现已购买课程与章节，收藏的优质课程、拥有的优惠券等，可以在线充值账户，更改密码、重新绑定账号等。

![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/154600_e09938e3_6569472.png "屏幕截图.png")

#### 安装教程

直接下载源码，复制到根目录下，配置好伪静态访问就可以啦~，省略掉一系列繁琐的安装步骤。赶紧下载体验吧~
![输入图片说明](https://images.gitee.com/uploads/images/2020/1214/171207_c98bd4cf_6569472.png "屏幕截图.png")

### 合作伙伴
![输入图片说明](https://images.gitee.com/uploads/images/2020/0725/120430_ab7fff0d_6569472.png "画板 1 拷贝 3(4).png")

### 版权信息

版权所有Copyright © 2015-2020 NiuShop开源商城&nbsp;版权所有

All rights reserved。
 
上海牛之云网络科技有限公司&nbsp;&nbsp;提供技术支持  


