<?php
/**
 * BaseController.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\lead\controller;

use data\service\GoodsCategory;
use data\service\GoodsBrand;
use data\service\Goods;
class ShopEx extends BaseController
{
    public function __construct()
    {
      parent::__construct();
      
    }
   /**
    *导入测试(non-PHPdoc)
    * @see \app\lead\controller\BaseController::test()
    */
    public function test(){
     
        $data = $this->table('category')->select();
    }
 /**
     *  导入商品分类
     */
    public function leadCategory(){
        $data = $this->table('category')->select();
        $goods_category = new GoodsCategory();
        if(!empty($data))
        {
            foreach ($data as $k => $v)
            {
                $category_id = $v['cat_id'];
                $category_name = $v['cat_name'];
                $short_name = $v['cat_name'];
                $pid = $v['parent_id'];
                $is_visible = 1;
                $key_words = $v['keywords'];
                $description = $v['cat_desc'];
                $goods_category->addOrEditGoodsCategory($category_id, $category_name, $short_name, $pid, $is_visible, $keywords, $description);
            }
        }
        return 1;
    }
    /**
     * 导入商品品牌
     */
    public function leadBrand(){
        $data = $this->table('brand')->select();
        $goods_brand = new GoodsBrand();
        if(!empty($data))
        {
            foreach ($data as $k => $v)
            {
                $brand_id = $v['brand_id'];
                $brand_name =  $v['brand_name'];
                $brand_initial = '';
                $brand_class = '';
                $brand_pic = 'upload/ecshop/'.$v['brand_logo'];
                $brand_recommend = 0;
                $sort = $v['sort_order'];
                $brand_ads = 0;
                $category_name = '';
                $category_id_1 = 0;
                $category_id_2 = 0;
                $category_id_3 = 0;
                $goods_brand->addOrUpdateGoodsBrand($brand_id, $brand_name, $brand_initial, $brand_class, $brand_pic, $brand_recommend, $sort, $brand_ads, $category_name, $category_id_1, $category_id_2, $category_id_3);
           
            }
        return 1;
        }
    
    }
    /**
     * 导入商品类型
     */
    public function leadAttribute(){
        $goods = new Goods();
        $attribute_list = $this->table('goods_type')->select();
        if(!empty($attribute_list))
        {
            foreach ($attribute_list as $k => $v)
            {
                $attribute_name = '';
                $is_use = '';
                $spec_id_array = '';
                $sort = '';
                $value_string = '';
                $goods->addAttributeService($attribute_name, $is_use, $spec_id_array, $sort, $value_string);
            }
        }
        
    }
}