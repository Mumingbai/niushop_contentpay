<?php
namespace app\workspc\controller;

use data\worksservice\Course;
use data\worksservice\Vmember;
use data\worksservice\VmemberAccount;
use data\service\Member as MemberService;


class Package extends BaseController
{
    
    /** 
     * 套餐列表页 
     * 
     **/
    public function packageList()
    {
        $course = new Course;
        
        $page = request()->get('page' , 1);
        $pagesize = 4;
        
        $order = request()->get('order','sort');
        if($order=='buy_count') $order = 'buy_count+basics_buy';
            
        $sort_type = request()->get('sort_type','asc');
        
        $condition['status']=array('in','1,2');
        
        $package_list = $course->getPackageList($page , $pagesize , $condition , "$order $sort_type" );
        $this->assign('package_list',$package_list['data']);
        
        
        $this->assign('page' , $page);
        $this->assign('page_count' , $package_list['page_count']);
        $this->assign('data' , request()->get());
        
        return view($this->style . 'Package/packageList');
    }
    
    /**
     * 套餐详情页
     *
     **/
    public function packageDetail()
    {
        $course = new Course;
        $package_id = request()->get('package_id');
        $package_info = $course->getPackageInfo($package_id);
        
        $package_course_list = $course->getPackageCourseList($package_id);
        
        $this->assign("package_info" , $package_info);
        $this->assign('package_course_list' , $package_course_list);
        
        $teacher_id_list = [];
        $lession_num = 0;
        if(!empty($package_course_list)){
            
            foreach($package_course_list as $key=>$val)
            {
                $lession_num += $val['course_info']['lession_num'];
                
                if(!empty($val['course_info']['course_teacher_list'])){
                    
                    foreach($val['course_info']['course_teacher_list'] as $ke=>$vo)
                    {
                        if(!in_array($vo['teacher_id'] , $teacher_id_list) && $vo['teacher_id'] != 0)
                        {
                            $teacher_id_list[] = $vo['teacher_id'];
                        }
                    }
                }  
            }
        }
        
        
        $teacher_id_list = join(',' , $teacher_id_list);
        $condition['id'] = ['in' , $teacher_id_list];
        $teacher_list = $course->getTeacherList(1 , 0 , $condition);
        $this->assign('teacher_list' , $teacher_list['data']);
        $this->assign('lession_num' , $lession_num);
        
        
        
        $recommend_list = $course->getPackageList(1 , 3 , '' , 'buy_count+basics_buy desc');
        $this->assign('recommend_list' , $recommend_list['data']);
        
        // 评论列表
        $account = new VmemberAccount;
        $condition_e['relation_id'] = $package_id;
        $condition_e['relation_type'] = 2;
        $condition_e['is_show'] = 1;
        $condition_e['reply_evaluate'] = 0;
         
        $data = $account->getMemberEvaluateList(1,0,$condition_e,'evaluate_id desc');
         
        
        $evaluate_list = $data['data'];
        
        foreach($evaluate_list as $key=>$val){
            $condition_r['relation_id'] = $package_id;
            $condition_r['relation_type'] = 2;
            $condition_r['is_show'] = 1;
            $condition_r['reply_evaluate'] = $val['evaluate_id'];
            $val['reply_list'] = $account-> getMemberEvaluateList(1,0,$condition_r,"evaluate_id desc")['data'];
            $val['praise_count'] = $account->setMemberDianzanCount(1, $val['evaluate_id']);
            foreach($val['reply_list'] as $ke=>$va){
                $va['praise_count'] = $account->setMemberDianzanCount(1, $va['evaluate_id']);
            }
        }
         
        $this->assign('evaluate_list',$evaluate_list);
        
        // 获取该套餐是否被收藏
        $account = new VmemberAccount();
        $is_collection = $account->isCourseCollection($package_id,2);
        $this->assign('is_collection',$is_collection);
        
        // 获取会员资料
        $member = new MemberService();
        $member_info = $member->getMemberDetail($this->instance_id);
        if(!$member_info){
            $member_info = [];
        }
        $this->assign('member_info', $member_info);
        
        // 会员是否拥有该课程
        $vmember_service = new Vmember();
        $is_have_package = $vmember_service->getVmemberHaveIsCourse($package_id , 2);
        $this->assign('is_have_package',$is_have_package);
        
        //dump($is_have_package);
        
        return view($this->style . 'Package/packageDetail');
    }
    
    
}