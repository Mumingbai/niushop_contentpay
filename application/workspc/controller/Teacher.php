<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\workspc\controller;

use data\worksservice\Course as Course;

/**
 * 
 * 名师页控制器
 */
class Teacher extends BaseController
{
    public $course;
    
    public function __construct()
    {
        $this->course = new Course();
        parent::__construct();
    }
    /**
     *
     * 名师列表页
     */
    public function teacherList(){
        
        // 获取分类数据
        $condition['is_visible']=1;
        $condition['parent_id']=0;
        $subject_list = $this->course->getSubjectList(1,0,$condition,'sort asc');
        
        $this->assign('subject_list', $subject_list['data']);
        
        // 获取课程分页数据
        $pagesize=8;
      
        $page = request()->get('page','');
        if(!$page){
            $page=1;
        }
        
        if(request()->get('sid')){
           $condition1['subject_id_1'] = request()->get('sid');
        }
        $teacher_list=$this->course->getTeacherList($page,$pagesize,$condition1);
        $this->assign("teacher_list",$teacher_list['data']);
        
        // 公共分页数据
        $this->assign('page_count', $teacher_list['page_count']);
        $this->assign('page', $page);
        
        // 做默认被选中所需数据
        $data=request()->get();
        $this->assign('data',$data);
        
        return view($this->style . 'Teacher/teacherList');
    }
    
    /**
     *
     * 名师详情页
     */
    public function teacherDetail(){
        
        //获取教师的介绍资料
        $teacher_id = request()->get("id");
        $teacher = $this->course->getTeacherInfo($teacher_id);
        $this->assign("teacher",$teacher);
        
        //获取教师的相关课程
        $course_list=$this->course->getTeacherByCourse($teacher_id);
        
        $this->assign('course_list',$course_list);

        return view($this->style . 'Teacher/teacherDetail');
    }
}
