<?php
/**
 * Member.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\workspc\controller;

use data\service\Config;
use data\service\Member\MemberAccount as MemberAccount;
use data\service\Member as MemberService;
use data\service\UnifyPay;
use data\service\Upload\QiNiu;
use think\Session;
use data\worksservice\Vmember;
use data\worksservice\Course;
use data\worksservice\VmemberAccount;
use data\worksservice\Operate;
use data\worksservice\Order as OrderService;


/**
 * 会员控制器
 * 创建人：李吉
 * 创建时间：2017-02-06 10:59:23
 */
class Member extends BaseController
{
    public $notice;
    
     public function __construct()
    {
        parent::__construct();
        // 如果没有登录的话让其先登录
        $this->checkLogin();
        // 查询登陆用户信息
        if (! request()->isAjax()) {
            $member = new MemberService();
            $member_info = $member->getMemberDetail($this->instance_id);
            if (! empty($member_info['user_info']['user_headimg'])) {
                $member_img = $member_info['user_info']['user_headimg'];
            } else {
                $member_img = '0';
            }
            
            $curs = request()->get('curs', '1');
            $this->assign('curs', $curs);
    
            $this->assign('member_img', $member_img);
            $this->assign('member_info', $member_info);
            $this->assign("cart_list", $cart_list);
            
        }
        // 是否开启验证码
        $web_config = new Config();
        $this->login_verify_code = $web_config->getLoginVerifyCodeConfig($this->instance_id);
        $this->assign("login_verify_code", $this->login_verify_code["value"]);
        // 是否开启通知
        $instance_id = 0;
        $noticeMobile = $web_config->getNoticeMobileConfig($instance_id);
        $noticeEmail = $web_config->getNoticeEmailConfig($instance_id);
        $this->notice['noticeEmail'] = $noticeEmail[0]['is_use'];
        $this->notice['noticeMobile'] = $noticeMobile[0]['is_use'];
        $this->assign("notice", $this->notice);
        

        
    }  
    
    public function _empty($name)
    {}
    
    /* 会员中心首页 */
    public function index()
    {
        //获取会员拥有的课程列表
        $member = new Vmember();

        $type=request()->get('type',1);
        $pagesize = 4;
        $page=request()->get('page',1);
        $course_list = $member->getVmemberHaveList($page,$pagesize,['relation_type'=>$type]);
        $this->assign('course_list',$course_list['data']);
        //dump($course_list['data']);
        //exit;
        
        //做分页所需数据
        $this->assign('page', $page);
        $this->assign('page_count', $course_list['page_count']);
        
        $this->assign('type' , $type);

        return view($this->style . 'Member/index');
    }
    /* 
     * 会员所拥有课程的详细资料
     * 
     **/
    public function courseInfo(){
        
        $member = new Vmember();
        $course = new Course();
        $have_id = request()->get('have_id');
        $course_id = request()->get('course_id');
        
        if($have_id){
            
            $have_info = $member->getVmemberHaveInfo($have_id);
        }else{
            $condition['uid'] = $this->uid;
            $condition['relation_type'] = 1;
            $condition['relation_id'] = $course_id;
            $have_info = $member->getVmemberHaveInfo('' , $condition);
        }
        
        $course_info = $course->getCourseInfo($have_info['relation_id']);
        
        if(empty($have_info['kpoint_id'])){
            $kpoint_list = $course->getCourseKpointList($have_info['relation_id']);
        }else{
            $condition_k['kpoint_id'] = ['in', $have_info['kpoint_id']];
            $kpoint_list = $course->getCourseKpointList('', $condition_k);
        }
        //dump($have_info);
        $this->assign("have_info",$have_info);
        $this->assign("course_info",$course_info);
        $this->assign("kpoint_list",$kpoint_list);
        
        return view($this->style . 'Member/courseInfo');
    }
    
    /*
     * 会员所拥有课程的详细资料
     *
     **/
    public function packageInfo(){
    
        $member = new Vmember();
        $course = new Course();
        $have_id = request()->get('have_id');
        $have_info = $member->getVmemberHaveInfo($have_id);
        $package_info = $course->getPackageInfo($have_info['relation_id']);
        $package_course_list = $course->getPackageCourseList($have_info['relation_id']);
        
        $teacher_name_list = [];
        $lession_num = 0;
        
        foreach($package_course_list as $key=>$val)
        {
            $lession_num += $val['course_info']['lession_num'];
            foreach($val['course_info']['course_teacher_list'] as $ke=>$vo)
            {
                if(!in_array($vo['teacher_name'] , $teacher_name_list))
                {
                    $teacher_name_list[] = $vo['teacher_name'];
                }
            }
        }
        
         
        $this->assign("have_info",$have_info);
        $this->assign("package_info",$package_info);
        $this->assign('package_course_list' , $package_course_list);
        $this->assign("teacher_name_list" , $teacher_name_list);
        
        return view($this->style . 'Member/packageInfo');
    }
    
    /*
     * 改变收藏状态
     *
     **/
    public function alterCollection(){
        
        $member = new VmemberAccount();
        $relation_id = request()->post('relation_id');
        $status = request()->post('status');
        $type = request()->post('type' , 1);
        
        if($status=="收藏"){
            
            $result = $member->addMemberCollection($type , $relation_id);
            return AjaxReturn($result);
            
        }else if($status=="取消收藏"){
            
            $result = $member->delMemberCollection($type , $relation_id);
            return AjaxReturn($result);
            
        }
    }
    
    /* 
     * 创建人：赵海雷
     * 我的笔记 
     **/
    public function courseNote()
    {
        // 列表数据
        $page = request()->get('page',1);
        $pagesize = 6;
        $condition = ['uid'=>$this->uid];
        if(request()->get('key')){
            $condition['course_title'] = ['like','%'.request()->get('key').'%'];
        }
        $member_account = new VmemberAccount;
        $note_list = $member_account->getMemberStudyNoteList($page,$pagesize,$condition,'note_id desc');
        $this->assign('note_list',$note_list['data']);
        //dump($note_list['data']);
        
        // 分页数据
        $this->assign('page',$page);
        $this->assign('page_count',$note_list['page_count']);
        $this->assign('key',request()->get('key'));
        
        return view($this->style . 'Member/courseNote');
    }
    
    /*
     * 创建人：赵海雷
     * 删除笔记
     **/
    public function delCourseNote(){
        
        $member_account = new VmemberAccount;
        $note_id = request()->post('note_id','');
        $res = $member_account->delMemberStudyNote($note_id);
        return $res;
        
    }
    
    /*
     * 创建人：赵海雷
     * 修改笔记
     **/
    public function updateCourseNote(){
    
        $course_id = request()->post('course_id','');
        $course_title = request()->post('course_title','');
        $kpoint_id = request()->post('kpoint_id', '');
        $note_text = request()->post('note_text', '');
        $note_id = request()->post('note_id', '');
        $works_play_time = request()->post('works_play_time', 0);
        
        $vmember_account_service = new VmemberAccount();
        $res = $vmember_account_service->addUpdateMemberStudyNote($note_id, $course_id, $course_title, $kpoint_id, $note_text, $works_play_time);
        return AjaxReturn($res);
        
        
    
    }
    
    /* 课程收藏 */
    public function myFavorites()
    {
        $type = request()->get("type" , 1);
        
        // 根据页数获取收藏列表
        $page = request()->get('page',1);
        
        $pagesize = 4;
        $account = new VmemberAccount();
        $collection_list = $account->getMemberCollectionList($page,$pagesize,['type'=>$type],'collection_id desc');
        $this->assign("collection_list" ,$collection_list['data']);
        //dump($collection_list['data']);
        
        // 做分页所需数据
        $this->assign("page",$page);
        $this->assign('page_count',$collection_list['page_count']);
        
        $this->assign("type" , $type);
        
        return view($this->style . 'Member/myFavorites');
    }
    
    /**
     * 创建人：赵海雷
     * 我的优惠劵 
     **/
    public function myCouPon()
    {   
        // 定义一个将打折数字转化为文字的方法
        function alter($num){
            $arr = ['零','一','二','三','四','五','六','七','八','九'];
            $num = (int)$num;
            if($num%10==0){
                $res = $arr[$num/10];
            }else{
                $n1 = floor($num/10);
                $n2 = $num%10;
                $res = $arr[$n1].$arr[$n2];
            }
            return $res;
        }
        
        // 优惠券的类型
        $type = request()->get('type', 0 );
        $condition['status'] = $type;
       
        // 获取我的优惠券列表
        $member_account = new VmemberAccount();
        $data = $member_account->getMemberCouponList(1,0,$condition,'create_time desc');
        $my_coupon_list = $data['data'];
        
         // 如果是打折类型的优惠券则将数字转换为文字
         foreach($my_coupon_list as $key=>$val){
            if($val['discount_type']==2){
                $val['type_value'] = alter($val['type_value']).'折';
            }
         }
         $this->assign('my_coupon_list',$my_coupon_list);
         
         //获取已发布的优惠券列表
         $coupon_service = new Operate();
         $release_coupon_list = $coupon_service->getProsceniumCouponList(1,0,'','');
         
         //查询会员是否已领取过优惠券 以及将打折类型的优惠券 数字转换为文字
         foreach($release_coupon_list['data'] as $k=>$v){
             
             $is_recieved_coupon = $member_account->getMemberCouponByCouponid($v['vcoupon_id']);
             if(!empty($is_recieved_coupon)){
                 $v['is_recieve_coupon'] = 1;
             }else{
                 $v['is_recieve_coupon'] = 0;
             }
             if($v['discount_type']==2){
                 $v['type_value'] = alter($v['type_value']).'折';
             }
         }
        
        $this->assign('release_coupon_list',$release_coupon_list['data']);
        $this->assign('type' , $type);
        //dump($release_coupon_list['data']);
        
        
        return view($this->style . 'Member/myCouPon');
    }
    
    
    /**
     * 领取优惠券 
     * 
     **/
    public function receiveCoupon(){
        
        $vc_id = request()->post('vc_id', '');
        $vmember_account = new VmemberAccount();
        $res = $vmember_account->addMemberCoupon($vc_id);
        return AjaxReturn($res);
    }
    
    
    /* 订单中心 */
    public function myOrderList()
    {
        $order_status = request()->get('status');
        $condition['uid']=$this->uid;
        if($order_status!=''){
            $condition['order_status'] = $order_status;
        }
        
        $page = request()->get('page',1);
        $pagesize = 4;
        $vorder_service = new OrderService();
        $order_list = $vorder_service->getOrderList($page,$pagesize,$condition,'create_time desc');
        $this->assign('order_list', $order_list['data']);
        //dump($order_list['data'][0]['item_list']);
        
        // 做默认与分页所需数据
        $this->assign('status',request()->get('status'));
        $this->assign('page',$page);
        $this->assign('page_count',$order_list['page_count']);
        
        return view($this->style . 'Member/myOrderList');
    }
    
    /** 
     * 
     * 取消订单 
     **/
    public function alterOrderStatus(){
        
        $v_order_id = request()->post('v_order_id', '');
        $v_order_service = new OrderService();
        $res = $v_order_service->setOrderStatusCancel($v_order_id);
        
        return AjaxReturn($res);
    }
    
    /**
     *
     * 订单详情
     **/
    public function orderInfo(){
    
        // 订单信息
        $v_order_id = request()->get("v_order_id");
        $order_service = new OrderService();
        $order_info = $order_service->getOrderInfo($v_order_id);
        $this->assign("order_info" , $order_info);
       
        
        //优惠劵
        $mc_id = $order_info['mc_id'];
        if($mc_id > 0)
        {
            $vmember_account = new VmemberAccount();
            $member_coupon_info = $vmember_account->getMemberCouponInfo($mc_id);
            $this->assign('member_coupon_info', $member_coupon_info);
        }
        
        
        //订单项列表
        $order_item = $order_service->getOrderItemList($v_order_id);
        $type = $order_item[0]['item_type'];
        $relation_id = $order_item[0]['relation_id'];
        
        // 关联的课程或套餐信息
        $course = new Course();
        if($type == "1"){
            
            $relation_info = $course->getCourseInfo($relation_id);
        }else{
            
            $relation_info = $course->getPackageInfo($relation_id);
        }
        //dump($relation_info);
        $this->assign("type" , $type);
        $this->assign("relation_info" , $relation_info);
        
    
        return view($this->style . 'Member/orderInfo');
    }
    
    /**
     * 我的账户
     */
    public function myAccount()
    {
        
        // 该店铺下的余额流水列表
        $page_index = request()->get('page', '1');
        $page_size = 10;
        
        $condition['nmar.uid'] = $this->uid;
        $condition['nmar.shop_id'] = $this->instance_id;
        $condition['nmar.account_type'] = 2;
        
        $list = $this->user->getAccountList($page_index, $page_size, $condition);
        $this->assign("balances", $list);
        
        // 用户在该店铺的账户余额总数
        $member = new MemberService();
        $member_info = $member->getMemberDetail($this->instance_id);
        $this->assign("sum", $member_info['balance']);
        
        // 余额充值
        $pay = new UnifyPay();
        $pay_no = $pay->createOutTradeNo();
        $this->assign("pay_no", $pay_no);
        
        // 分页数据
        $this->assign('page_count', $list['page_count']);
        $this->assign('page', $page_index);
        
         return view($this->style . 'Member/myAccount');
    }
    /**
     * 创建充值订单
     */
    public function createRechargeOrder()
    {
        $recharge_money = request()->post('recharge_money', 0);
        $out_trade_no = request()->post('out_trade_no', '');
        if (empty($recharge_money) || empty($out_trade_no)) {
            return AjaxReturn(0);
        } else {
            $member = new MemberService();
            $retval = $member->createMemberRecharge($recharge_money, $this->uid, $out_trade_no);
            return AjaxReturn($retval);
        }
    }
    /** 
     * 我的积分 
     */
    public function myIntegra()
    {
        $shop_id = $this->instance_id;
        $conponAccount = new MemberAccount();
        $start_time = request()->post('start_time', '2016-01-01');
        $end_time = request()->post('end_time', '2099-01-01');
        $page_index = request()->get('page', '1');
        // 每页显示几个
        $page_size = 10;
        $condition['nmar.uid'] = $this->uid;
        $condition['nmar.shop_id'] = $shop_id;
        $condition['nmar.account_type'] = 1;
        // 查看用户在该商铺下的积分消费流水
        $list = $this->user->getAccountList($page_index, $page_size, $condition);
        // $list = $this->user->getPageMemberPointList($start_time, $end_time, $page_index, $page_count, $shop_id);
        foreach ($list["data"] as $list2) {
            // if ($list2["number"] < 0) {
            // $list2["number"] = 0 - $list2["number"];
            // }
            $list2["number"] = (int) $list2["number"];
            $list2["data_id"] = $this->user->getOrderNumber($list2["data_id"])["out_trade_no"];
        }
        // 获取兑换比例
        $account = new MemberAccount();
        $accounts = $account->getConvertRate($shop_id);
        // 查看积分总数
        $account_type = 1;
        
        $conponSum = $conponAccount->getMemberAccount($shop_id, $this->uid, $account_type);
        // 店铺名称
        // $shop_name = $this->user->getShopNameByShopId($shop_id);
        $shop_name = $this->user->getWebSiteInfo();
        $this->assign([
            'account' => $accounts['convert_rate'],
            "sum" => (int) $conponSum,
            "shopname" => $shop_name['title'],
            "shop_id" => $shop_id,
            'page_count' => $list['page_count'],
            'total_count' => $list['total_count'],
            "balances" => $list,
            'page' => $page_index
        ]);
        
        $member_detail = $this->user->getMemberDetail($this->instance_id);
        $this->assign("member_detail", $member_detail);
        return view($this->style . 'Member/myIntegra');
    }
    /* 邀请好友 */
    public function myInvite()
    {
    
        return view($this->style . 'Member/myInvite');
    }
    /* 个人资料 */
    public function personalData()
    {

        if ($_FILES) {
             
            $file_name = date("YmdHis") . rand(0, date("is")); // 文件名
            $ext = explode(".", $_FILES["user_headimg"]["name"]);
            $file_name .= "." . $ext[1];
            
            // 检测文件夹是否存在，不存在则创建文件夹
            $path = 'upload/avator/';
            if (! file_exists($path)) {
                $mode = intval('0777', 8);
                mkdir($path, $mode, true);
            }
            
            $img_result = $this->moveUploadFile($_FILES["user_headimg"]["tmp_name"], $path . $file_name);
            if ($img_result["code"]) {
                
                $user_headimg = $img_result["path"];
                $upload_headimg_status = $this->user->updateMemberInformation("", "", "", "", "", "", $user_headimg);
                $this->redirect(__URL('WORKS_MAIN/Member/personalData'));
            } else {
                
                $this->error("头像上传失败!");
            }     
            
        }
        
        $member_detail = $this->user->getMemberDetail($this->instance_id);
        $member_info = $this->user->getMemberDetail();
        
        if ($member_info['user_info']['birthday'] == 0 || $member_info['user_info']['birthday'] == "") {
            $member_info['user_info']['birthday'] = "";
        } else {
            $member_info['user_info']['birthday'] = date('Y-m-d', $member_info['user_info']['birthday']);
        }
        $this->assign('member_info', $member_info);
        
        $member_img = $member_info['user_info']['user_headimg'];
        
        $this->assign('member_img', $member_img);
        $this->assign('member_detail', $member_detail);
        
        return view($this->style . 'Member/personalData');
    }
    /** 
     * 
     * 修改用户资料
     **/
    public function updateUser(){
        
        $user_name = request()->post('user_name', '');
        $user_qq = request()->post('user_qq', '');
        $real_name = request()->post('real_name', '');
        $sex = request()->post('sex', '');
        $birthday = request()->post('birthday', '');
        $location = request()->post('location', '');
        $birthday = date('Y-m-d', strtotime($birthday));
        
        // 把从前台显示的内容转变为可以存储到数据库中的数据
        $res = $this->user->updateMemberInformation($user_name, $user_qq, $real_name, $sex, $birthday, $location, "");
        return $res;
    }
    /**
     * 图片上传
     *
     * @param unknown $file_path
     * @param unknown $key
     */
    public function moveUploadFile($file_path, $key)
    {
        $config = new Config();
        
        $upload_type = $config->getUploadType(0);
        if ($upload_type == 1) {
            $ok = @move_uploaded_file($file_path, $key);
            $result = [
                "code" => $ok,
                "path" => $key,
                "domain" => '',
                "bucket" => ''
            ];
        } elseif ($upload_type == 2) {
            $qiniu = new QiNiu();
            $result = $qiniu->setQiniuUplaod($file_path, $key);
        }
        return $result;
    }
    
    /* 绑定更改 */
    public function threelogin()
    {   
        if (request()->isGet()) {
            $atc = request()->get('atc', '');
            $this->assign('atc', $atc);
        }
        $member_detail = $this->user->getMemberDetail($this->instance_id);
        $this->assign("member_detail", $member_detail);
        return view($this->style . 'Member/threelogin');
    }
    
    /**
     * 功能：绑定手机
     * 创建人：李志伟
     * 创建时间：2017年2月16日17:17:43
     */
    public function modifyMobile()
    {
        if (request()->isAjax()) {
            
            $uid = $this->user->getSessionUid();
            $mobile = request()->post('mobile', '');
            $mobile_code = request()->post('mobile_code', '');
            
            if ($this->notice['noticeMobile'] == 1) {
                $verification_code = Session::get('mobileVerificationCode');
                if ($mobile_code == $verification_code && ! empty($verification_code)) {
                    $member = new MemberService();
                    $retval = $member->modifyMobile($uid, $mobile);
                    if ($retval == 1)
                        Session::delete('mobileVerificationCode');
                    return AjaxReturn($retval);
                } else {
                    return array(
                        'code' => 0,
                        'message' => '手机验证码输入错误'
                    );
                }
            } else {
                // 获取手机是否被绑定
                $member = new MemberService();
                $is_bin_mobile = $member->memberIsMobile($mobile);
                if ($is_bin_mobile) {
                    return array(
                        'code' => 0,
                        'message' => '该手机号已存在'
                    );
                } else {
                    $member = new MemberService();
                    $retval = $member->modifyMobile($uid, $mobile);
                    return AjaxReturn($retval);
                }
            }
        }
    }
    
    /**
     * 功能：绑定邮箱
     * 创建人：李志伟
     * 创建时间：2017年2月16日17:17:43
     */
    public function modifyEmail()
    {
        $member = new MemberService();
        $uid = $this->user->getSessionUid();
        $email = request()->post('email', '');
        $email_code = request()->post('email_code', '');
        if ($this->notice['noticeEmail'] == 1) {
            $verification_code = Session::get('emailVerificationCode');
            if ($email_code == $verification_code && ! empty($verification_code)) {
                $member = new MemberService();
                $retval = $member->modifyEmail($uid, $email);
                if ($retval == 1)
                    Session::delete('emailVerificationCode');
                return AjaxReturn($retval);
            } else {
                return array(
                    'code' => 0,
                    'message' => '邮箱验证码输入错误'
                );
            }
        } else {
            // 获取邮箱是否被绑定
            $member = new MemberService();
            $is_bin_email = $member->memberIsEmail($email);
            if ($is_bin_email) {
                return array(
                    'code' => 0,
                    'message' => '该邮箱已存在'
                );
            } else {
                $member = new MemberService();
                $retval = $member->modifyEmail($uid, $email);
                return AjaxReturn($retval);
            }
        }
    }
    /**
     * 密码设置 
     */
    public function initUpdateUser()
    {   
        if (request()->isAjax()) {
            $member = new MemberService();
            $uid = $this->user->getSessionUid();
            $old_password = request()->post('old_password', '');
            $new_password = request()->post('new_password', '');
            $retval = $member->ModifyUserPassword($uid, $old_password, $new_password);
            return AjaxReturn($retval);
        }
        $member_detail = $this->user->getMemberDetail($this->instance_id);
        $this->assign("member_detail", $member_detail);
       
        return view($this->style . 'Member/initUpdateUser');
    }
    /**
     * 消息中心 
     */
    public function letter()
    {
    
        return view($this->style . 'Member/letter');
    }
    /**
     * 检测用户
     */
    private function checkLogin()
    {
        $uid = $this->user->getSessionUid();
        if (empty($uid)) {
    
            $_SESSION['login_pre_url'] = __URL(__URL__ . $_SERVER['PATH_INFO']);
            $redirect = __URL("WORKS_MAIN/index/index");
            $this->redirect($redirect);
        }
        $is_member = $this->user->getSessionUserIsMember();
        if (empty($is_member)) {
            $redirect = __URL("WORKS_MAIN/index/index");
            $this->redirect($redirect);
        }
    }
    /** 
     * 退出登录
     */
     public function logOut()
    {
        $member = new MemberService();
        
        $member->Logout();
        return AjaxReturn(1);
       
    } 
    /**
     * 验证码
     *
     * @return multitype:number string
     */
    public function verify()
    {
        $vertification = request()->post('vertification', '');
        if (! captcha_check($vertification)) {
            $retval = [
                'code' => 0,
                'message' => "验证码错误"
            ];
        } else {
            $retval = [
                'code' => 1,
                'message' => "验证码正确"
            ];
        }
        return $retval;
    }
    
}
