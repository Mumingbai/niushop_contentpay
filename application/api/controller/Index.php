<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\api\controller;

use data\service\Config;
use data\extend\WchatOauth;
use data\service\GoodsCategory;
use data\service\Platform;
use data\service\GoodsBrand;
use data\service\Goods;
class Index extends BaseController
{
    function __construct()
    {
        parent::__construct();
    }
       
    /**
     * 首页广告位展示
     * @return multitype:unknown
     */
    public function getIndexAdvPosition(){
        $title = "首页广告微展示,adv_index:首页轮播广告位,adv_new:首页新品推荐下广告位,adv_brand:首页商品推荐广告位";
        // 首页轮播图
        $platform = new Platform();
        $plat_adv_list = $platform->getPlatformAdvPositionDetail(1105);
        // 首页新品推荐下方广告位
        $index_adv_one = $platform->getPlatformAdvPositionDetail(1188);
        // 首页品牌推荐下方广告位
        $index_adv_two = $platform->getPlatformAdvPositionDetail(1189);
        $data = array(
            'adv_index' => $plat_adv_list,
            'adv_new'   => $index_adv_one,
            'adv_brand' => $index_adv_two
        );
        return $this->outMessage($title, $data);
    }
    /**
     * 获取分享相关票据
     */
    public function getShareTicket($url)
    {
        
        $title = "获取微信票据";
        $config = new Config();
        $auth_info = $config->getInstanceWchatConfig(0);
        // 获取票据
        if (! empty($auth_info['value']['appid'])) {
            // 针对单店版获取微信票据
            $wexin_auth = new WchatOauth();
            $signPackage['appId'] = $auth_info['value']['appid'];
            $signPackage['jsTimesTamp'] = time();
            $signPackage['jsNonceStr'] = $wexin_auth->get_nonce_str();
            $jsapi_ticket = $wexin_auth->jsapi_ticket();
            $signPackage['ticket'] = $jsapi_ticket;
            $Parameters = "jsapi_ticket=" . $signPackage['ticket'] . "&noncestr=" . $signPackage['jsNonceStr'] . "&timestamp=" . $signPackage['jsTimesTamp'] . "&url=" . $url;
            $signPackage['jsSignature'] = sha1($Parameters);
            $this->outMessage($title, $signPackage);
        } else {
            $signPackage = array(
                'appId' => '',
                'jsTimesTamp' => '',
                'jsNonceStr' => '',
                'ticket' => '',
                'jsSignature' => ''
            );
             $this->outMessage($title, $signPackage, '-9001', "当前微信没有配置!");
        }
    }
    /**
     * 获取首页相关推荐商品
     */
    public function getIndexReconmmendGoods(){
        $title="获取首页相关推荐商品,goods_category_block:首页商品分类楼层,goods_platform_recommend:首页推荐商品列表,goods_brand_list:首页品牌相关列表，显示6个,current_time:当前时间,goods_hot_list:首页商城热卖,goods_recommend_list:首页商城推荐商品列表,goods_discount_list:首页限时周口商品列表";
        //首页商品分类楼层
        $shop_id = 0;
        $good_category = new GoodsCategory();
        $block_list = $good_category->getGoodsCategoryBlockQuery($shop_id, 4);
        
        // 首页新品推荐列表
        $goods_platform = new Platform();
        $goods_platform_list = $goods_platform->getRecommendGoodsList($shop_id, 4);
        
        // 品牌列表
        $goods_brand = new GoodsBrand();
        $goods_brand_list = $goods_brand->getGoodsBrandList(1, 6, '', 'sort');
        
        // 限时折扣列表
        $goods = new Goods();
        $condition['status'] = 1;
        $condition['ng.state'] = 1;
        $discount_list = $goods->getDiscountGoodsList(1, 6, $condition, 'end_time');
        
        foreach ($discount_list['data'] as $k => $v) {
            $v['discount'] = str_replace('.00', '', $v['discount']);
        }
        // 获取当前时间
        $current_time = $this->getCurrentTime();
        
        // 首页商城热卖
        $val['is_hot'] = 1;
        $goods_hot_list = $goods_platform->getPlatformGoodsList(1, 0, $val);
        
        // 首页商城推荐
        $val1['is_recommend'] = 1;
        $goods_recommend_list = $goods_platform->getPlatformGoodsList(1, 0, $val1);
        $data = array(
            'goods_category_block' => $block_list,
            'goods_platform_recommend' => $goods_platform_list,
            'goods_brand_list' => $goods_brand_list['data'],
            'goods_discount_list' =>$discount_list['data'],
            'current_time'        => $current_time,
            'goods_hot_list'      => $goods_hot_list['data'],
            'goods_recommend_list' => $goods_recommend_list['data']
        );
        return $this->outMessage($title, $data);
    }
    /**
     * 获取限时折扣相关数据
     */
    public function getDiscountData(){
        $title="获取限时折扣相关数据,discount_adv:限时折扣广告位,goods_category_list:限时折扣需要查询一级分类,current_time:获取当前时间";
        // 限时折扣广告位
        $platform = new Platform();
        $discounts_adv = $platform->getPlatformAdvPositionDetail(1163);
        //限时折扣商品一级分类数据
        $goods_category = new GoodsCategory();
        $goods_category_list_1 = $goods_category->getGoodsCategoryList(1, 0, [
            "is_visible" => 1,
            "level" => 1
        ]);
        $current_time = $this->getCurrentTime();
        $data = array(
            'discount_adv' => $discounts_adv,
            'goods_category_list' => $goods_category_list_1,
            'current_time' => $current_time
        );
        $this->outMessage($title, $data);
    }
    /**
     * 获取限时折扣页面商品数据
     */
    public function getDiscountGoods(){
        $title = "获取限时折扣的商品列表，需要必填参数对应商品分类category_id";
        //对应商品分类id
        $category_id = request()->request('category_id', '0');
        if(empty($category_id))
        {
            return $this->outMessage($title, '','-50','缺少必填参数category_id');
        }
        //对应分页
        $page_index = request()->request("page",1);
        $goods = new Goods();
        $condition['status'] = 1;
        $condition['ng.state'] = 1;
        if (! empty($category_id)) {
            $condition['category_id_1'] = $category_id;
        }
        $discount_list = $goods->getDiscountGoodsList($page_index, PAGESIZE, $condition, "ng.sort desc,ng.create_time desc");
        foreach ($discount_list['data'] as $k => $v) {
            $v['discount'] = str_replace('.00', '', $v['discount']);
            $v['promotion_price'] = str_replace('.00', '', $v['promotion_price']);
            $v['price'] = str_replace('.00', '', $v['price']);
        }
        return $this->outMessage($title, $discount_list);
    }
    
    
}
