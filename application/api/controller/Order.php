<?php
/**
 * Order.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\api\controller;

use data\model\AlbumPictureModel;
use data\model\NsCartModel;
use data\model\NsGoodsModel;
use data\service\Config;
use data\service\Express;
use data\service\Goods;
use data\service\Member;
use data\service\Member as MemberService;
use data\service\Order\Order as OrderOrderService;
use data\service\Order\OrderGoods;
use data\service\Order as OrderService;
use data\service\promotion\GoodsExpress as GoodsExpressService;
use data\service\promotion\GoodsMansong;
use data\service\Promotion;
use data\service\promotion\GoodsPreference;
use data\service\Shop;
use think\Request;

/**
 * 订单控制器
 *
 * @author Administrator
 *        
 */
class Order extends BaseController
{
    /**
     * 获取订单相关数据
     */
    public function getOrderData(){
        $title = "获取订单类相关数据";
        $goods_sku_list = request()->request("goods_sku_list", '');
        if(empty($goods_sku_list))
        {
            return $this->outMessage($title, "",'-50', "缺少必填参数goods_sku_list");
        }
       if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $order = new OrderService();
        $shop_service = new Shop();
        /******************************************物流地址以及运费******************************************************/
        $express = 0;//运费
        $member = new MemberService();
        $goods_express_service = new GoodsExpressService();
        $address = $member->getDefaultExpressAddress(); // 获取默认收货地址
        $express_company_list = array();
        if (! empty($address)) {
            // 物流公司
            $express_company_list = $goods_express_service->getExpressCompany($this->instance_id, $goods_sku_list, $address['province'], $address['city'], $address['district']);
            if (! empty($express_company_list)) {
                foreach ($express_company_list as $v) {
                    $express = $v['express_fee']; // 取第一个运费，初始化加载运费
                    break;
                }
            }
        }
        /***************************************************************优惠金额**************************************/
        $goods_mansong = new GoodsMansong();
        $discount_money = $goods_mansong->getGoodsMansongMoney($goods_sku_list);
        /**************************************************************订单总金额***************************************/
        $count_money = $order->getGoodsSkuListPrice($goods_sku_list);
        /*************************************************************自提服务费*****************************************/
        $pick_up_money = $order->getPickupMoney($count_money);
        /************************************************************订单中商品列表信息**************************************/
        $goods_list = $this->getGoodsListBySkuList($goods_sku_list);
        /**************************************************************发票相关配置信息*************************************/
        $Config = new Config();
        $shop_config = $Config->getShopConfig(0);
        $order_invoice_content = explode(",", $shop_config['order_invoice_content']);
        $shop_config['order_invoice_content_list'] = array();
        foreach ($order_invoice_content as $v) {
            if (! empty($v)) {
                array_push($shop_config['order_invoice_content_list'], $v);
            }
        }
        /*****************************************************************会员账户余额信息********************************/
        $member_account = $member->getMemberAccount($this->uid, $this->instance_id);
        if ($member_account['balance'] == '' || $member_account['balance'] == 0) {
            $member_account['balance'] = '0.00';
        }
        /****************************************************************会员优惠券信息*********************************/
        $coupon_list = $order->getMemberCouponList($goods_sku_list);
        /****************************************************************满额包邮相关信息*********************************/
        $promotion = new Promotion();
        $promotion_full_mail = $promotion->getPromotionFullMail($this->instance_id);
        if (! empty($address)) {
            $no_mail = checkIdIsinIdArr($address['city'], $promotion_full_mail['no_mail_city_id_array']);
            if ($no_mail) {
                $promotion_full_mail['is_open'] = 0;
            }
        }
        /****************************************************************自提点信息***************************************/
        $pickup_point_list = $shop_service->getPickupPointList();
        $data = array(
            'member_address' => $address, //会员地址
            'express_company_list' => $express_company_list,  //物流公司地址
            'discount_money'  => $discount_money,  //优惠金额
            'order_total_money'  => $count_money,   //订单总金额
            'pick_up_money'      => $pick_up_money,  //自提点运费
            'goods_list'         => $goods_list,     //商品信息
            'shop_invoice_content' => $shop_config,  //发票等相关信息
            'member_account'       => $member_account, //会员账户信息
            'coupon_list'          => $coupon_list,    //会员可使用优惠券
            'promotion_full_mail'  => $promotion_full_mail  //满额包邮相关信息
        );
        return $this->outMessage($title, $data);
    }
    /**
     * 获取商品列表信息通过sku列表
     * @param unknown $order_sku_list
     * @return multitype:unknown \think\static Ambigous <unknown, number>
     */
    private function getGoodsListBySkuList($order_sku_list)
    {
        $cart_list = array();
        $sku_id = $order_sku_list[0];
        $num = $order_sku_list[1];
    
        // 获取商品sku信息
        $goods_sku = new \data\model\NsGoodsSkuModel();
        $sku_info = $goods_sku->getInfo([
            'sku_id' => $sku_id
        ], '*');
    
        // 查询当前商品是否有SKU主图
        $order_goods_service = new OrderGoods();
        $picture = $order_goods_service->getSkuPictureBySkuId($sku_info);
    
        // 清除非法错误数据
        $cart = new NsCartModel();
        if (empty($sku_info)) {
            $cart->destroy([
                'buyer_id' => $this->uid,
                'sku_id' => $sku_id
            ]);
        }
        $goods = new NsGoodsModel();
        $goods_info = $goods->getInfo([
            'goods_id' => $sku_info["goods_id"]
        ], 'max_buy,state,point_exchange_type,point_exchange,picture,goods_id,goods_name');
    
        $cart_list["stock"] = $sku_info['stock']; // 库存
        $cart_list["sku_id"] = $sku_info["sku_id"];
        $cart_list["sku_name"] = $sku_info["sku_name"];
    
        $goods_preference = new GoodsPreference();
        $member_price = $goods_preference->getGoodsSkuMemberPrice($sku_info['sku_id'], $this->uid);
        $cart_list["price"] = $member_price < $sku_info['promote_price'] ? $member_price : $sku_info['promote_price'];
    
        $cart_list["goods_id"] = $goods_info["goods_id"];
        $cart_list["goods_name"] = $goods_info["goods_name"];
        $cart_list["max_buy"] = $goods_info['max_buy']; // 限购数量
        $cart_list['point_exchange_type'] = $goods_info['point_exchange_type']; // 积分兑换类型 0 非积分兑换 1 只能积分兑换
        $cart_list['point_exchange'] = $goods_info['point_exchange']; // 积分兑换
        $cart_list["num"] = $num;
        // 如果购买的数量超过限购，则取限购数量
        if ($goods_info['max_buy'] != 0 && $goods_info['max_buy'] < $num) {
            $num = $goods_info['max_buy'];
        }
        // 如果购买的数量超过库存，则取库存数量
        if ($sku_info['stock'] < $num) {
            $num = $sku_info['stock'];
        }
        // 获取图片信息
        $album_picture_model = new AlbumPictureModel();
        $picture_info = $album_picture_model->get($picture == 0 ? $goods_info['picture'] : $picture);
        $cart_list['picture_info'] = $picture_info;
        $list[] = $cart_list;
        $goods_sku_list = $sku_id . ":" . $num; // 商品skuid集合
        $res["list"] = $list;
        $res["goods_sku_list"] = $goods_sku_list;
        return $res;
    }
    

    /**
     * 创建订单
     */
    public function orderCreate()
    {
        $title = "创建订单";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $title = "创建订单";
        $order = new OrderService();
        // 获取支付编号
        $out_trade_no = $order->getOrderTradeNo();
        $use_coupon = request()->request('use_coupon', 0); // 优惠券
        $integral = request()->request('integral', 0); // 积分
        $goods_sku_list = request()->request('goods_sku_list', ''); // 商品列表
        $leavemessage = request()->request('leavemessage', ''); // 留言
        $user_money = request()->request("account_balance", 0); // 使用余额
        $pay_type = request()->request("pay_type", 1); // 支付方式
        $buyer_invoice = request()->request("buyer_invoice", ""); // 发票
        $pick_up_id = request()->request("pick_up_id", 0); // 自提点
        $shipping_company_id = request()->request("shipping_company_id", 0); // 物流公司
        
        $shipping_type = 1; // 配送方式，1：物流，2：自提
        if ($pick_up_id != 0) {
            $shipping_type = 2;
        }
        $member = new Member();
        $address = $member->getDefaultExpressAddress();
        $shipping_time = date("Y-m-d H::i:s", time());
        $order_id = $order->orderCreate('1', $out_trade_no, $pay_type, $shipping_type, '1', 1, $leavemessage, $buyer_invoice, $shipping_time, $address['mobile'], $address['province'], $address['city'], $address['district'], $address['address'], $address['zip_code'], $address['consigner'], $integral, $use_coupon, 0, $goods_sku_list, $user_money, $pick_up_id, $shipping_company_id);
        if ($order_id > 0) {
            $order->deleteCart($goods_sku_list, $this->uid);
            $data = array(
                'out_trade_no' => $out_trade_no
            );
            return $this->outMessage($title, $data);
        } else {
            $data = array(
                'order_id' => $order_id
            );
            return $this->outMessage($title, $data);
        }
    }

    /**
     * 获取当前会员的订单列表
     */
    public function getOrderList()
    {
        $title = "获取会员订单列表 status空表示全部0，1,2,3包表示状态";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $page_index = request()->request("page", 1);
        $status = request()->request('status', 'all');
        
            $condition['buyer_id'] = $this->uid;
            $condition['is_deleted'] = 0;
            if (! empty($this->shop_id)) {
                $condition['shop_id'] = $this->shop_id;
            }
            
            if ($status != 'all') {
                switch ($status) {
                    case 0:
                        $condition['order_status'] = 0;
                        break;
                    case 1:
                        $condition['order_status'] = 1;
                        break;
                    case 2:
                        $condition['order_status'] = 2;
                        break;
                    case 3:
                        $condition['order_status'] = array(
                            'in',
                            '3,4'
                        );
                        break;
                    case 4:
                        $condition['order_status'] = array(
                            'in',
                            [
                                - 1,
                                - 2
                            ]
                        );
                        break;
                    case 5:
                        $condition['order_status'] = array(
                            'in',
                            '3,4'
                        );
                        $condition['is_evaluate'] = array(
                            'in',
                            '0,1'
                        );
                        break;
                    default:
                        break;
                }
            }
          
            // 还要考虑状态逻辑
            $order = new OrderService();
            $order_list = $order->getOrderList($page_index, PAGESIZE, $condition, 'create_time desc');
            return $this->outMessage($title, $order_list);
    }

  

    /**
     * 订单详情
     *
     * @return Ambigous <\think\response\View, \think\response\$this, \think\response\View>
     */
    public function orderDetail()
    {
        $title = "获取订单详情";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $order_id = request()->request('order_id', 0);
        $order_service = new OrderService();
        $detail = $order_service->getOrderDetail($order_id);
      if(empty($detail))
        {
            return $this->outMessage($title, "",'-50', "无法获取订单信息");
        }
        return $this->outMessage($title, $detail);
        
    }

    /**
     * 查询包裹物流信息
     * 2017年6月24日 10:42:34 王永杰
     */
    public function getOrderGoodsExpressMessage()
    {
        $title = "物流包裹信息";
        $express_id = request()->request("express_id", 0); // 物流包裹id
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $res = - 1;
        if ($express_id) {
            $order_service = new OrderService();
            $res = $order_service->getOrderGoodsExpressMessage($express_id);
            $res = array_reverse($res);
        }
        return $this->outMessage($title, $res);
    }

    /**
     * 订单项退款详情
     */
    public function refundDetail()
    {
        $title = "退款详情";
        $order_goods_id = request()->request('order_goods_id', 0);
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        if (empty($order_goods_id)) {
               return $this->outMessage($title, "",'-50', "缺少必填参数order_goods_id");
        }
        $order_service = new OrderService();
        $detail = $order_service->getOrderGoodsRefundInfo($order_goods_id);
        $this->assign("order_refund", $detail);
        $refund_money = $order_service->orderGoodsRefundMoney($order_goods_id);
        $this->assign('refund_money', $refund_money);
        $this->assign("detail", $detail);
        // 查询店铺默认物流地址
        $express = new Express();
        $address = $express->getDefaultShopExpressAddress($this->instance_id);
        // 查询商家地址
        $shop_info = $order_service->getShopReturnSet($this->instance_id);
        $data = array(
            'refund_detail' => $detail,
            'refund_money' => $refund_money,
            'shop_espress_address' => $address,
            'shop_address'         => $shop_info
        );
        return $this->outMessage($title, $data);
    }

    /**
     * 申请退款
     */
    public function orderGoodsRefundAskfor()
    {
        $title = "申请退款";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $order_id = request()->request('order_id', 0);
        $order_goods_id = request()->request('order_goods_id', 0);
        $refund_type = request()->request('refund_type', 1);
        $refund_require_money = request()->request('refund_require_money', 0);
        $refund_reason = request()->request('refund_reason', '');
        $order_service = new OrderService();
        $retval = $order_service->orderGoodsRefundAskfor($order_id, $order_goods_id, $refund_type, $refund_require_money, $refund_reason);
        return $this->outMessage($title, $retval);
    }

    /**
     * 买家退货
     *
     * @return Ambigous <multitype:unknown, multitype:unknown unknown string >
     */
    public function orderGoodsRefundExpress()
    {

        $title = "卖家退货";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $order_id = request()->request('order_id', 0);
        $order_goods_id = request()->request('order_goods_id', 0);
        $refund_express_company = request()->request('refund_express_company', '');
        $refund_shipping_no = request()->request('refund_shipping_no', 0);
        $refund_reason = request()->request('refund_reason', '');
        $order_service = new OrderService();
        $retval = $order_service->orderGoodsReturnGoods($order_id, $order_goods_id, $refund_express_company, $refund_shipping_no);
        return $this->outMessage($title, $retval);
    }

    /**
     * 交易关闭
     */
    public function orderClose()
    {
        $title = "关闭订单";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $order_service = new OrderService();
        $order_id = request()->request('order_id', '');
        $res = $order_service->orderClose($order_id);
         return $this->outMessage($title, $res);
    }

    /**
     * 收货
     */
    public function orderTakeDelivery()
    {
        $title = "订单收货";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $order_service = new OrderService();
        $order_id = request()->request('order_id', '');
        $res = $order_service->OrderTakeDelivery($order_id);
          return $this->outMessage($title, $res);
    }

    /**
     * 删除订单
     */
    public function deleteOrder()
    {
        $title = "订单收货";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
      
            $order_service = new OrderService();
            $order_id = request()->request("order_id", "");
            if(empty($order_id))
            {
                return $this->outMessage($title, "",'-50', "缺少必填参数order_id");
            }
            $res = $order_service->deleteOrder($order_id, 2, $this->uid);
            return $this->outMessage($title, $res);
      
    }
    /**
     * 商品评价提交
     * 创建：李吉
     * 创建时间：2017-02-16 15:22:59
     */
    public function addGoodsEvaluate()
    {
        $title = "评价商品提交";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $order = new OrderService();
        $order_id = request()->request('order_id', '');
        $order_no = request()->request('order_no', '');
        $order_id = intval($order_id);
        $order_no = intval($order_no);
        $goods = request()->request('goodsEvaluate', '');
        $goodsEvaluateArray = json_decode($goods);
        $dataArr = array();
        foreach ($goodsEvaluateArray as $key => $goodsEvaluate) {
            $orderGoods = $order->getOrderGoodsInfo($goodsEvaluate->order_goods_id);
            $data = array(
    
                'order_id' => $order_id,
                'order_no' => $order_no,
                'order_goods_id' => intval($goodsEvaluate->order_goods_id),
    
                'goods_id' => $orderGoods['goods_id'],
                'goods_name' => $orderGoods['goods_name'],
                'goods_price' => $orderGoods['goods_money'],
                'goods_image' => $orderGoods['goods_picture'],
                'shop_id' => $orderGoods['shop_id'],
                'shop_name' => "默认",
                'content' => $goodsEvaluate->content,
                'addtime' => time(),
                'image' => $goodsEvaluate->imgs,
                // 'explain_first' => $goodsEvaluate->explain_first,
                'member_name' => $this->user->getMemberDetail()['member_name'],
                'explain_type' => $goodsEvaluate->explain_type,
                'uid' => $this->uid,
                'is_anonymous' => $goodsEvaluate->is_anonymous,
                'scores' => intval($goodsEvaluate->scores)
            );
            $dataArr[] = $data;
        }
    
        $retval =  $order->addGoodsEvaluate($dataArr, $order_id);
        return $this->outMessage($title, $retval);
    }
    
    /**
     * 商品-追加评价提交数据
     * 创建：李吉
     * 创建时间：2017-02-16 15:22:59
     */
    public function addGoodsEvaluateAgain()
    {
        $title = "追评商品提交";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $order = new OrderService();
        $order_id = request()->request('order_id', '');
        $order_no = request()->request('order_no', '');
        $order_id = intval($order_id);
        $order_no = intval($order_no);
        $goods = request()->request('goodsEvaluate', '');
        $goodsEvaluateArray = json_decode($goods);
    
        $result = 1;
        foreach ($goodsEvaluateArray as $key => $goodsEvaluate) {
            $res = $order->addGoodsEvaluateAgain($goodsEvaluate->content, $goodsEvaluate->imgs, $goodsEvaluate->order_goods_id);
            if ($res == false) {
                $result = false;
                break;
            }
        }
        if ($result == 1) {
            $data = array(
                'is_evaluate' => 2
            );
            $result = $order->modifyOrderInfo($data, $order_id);
        }
    
        return $this->outMessage($title, $result);
    }
}