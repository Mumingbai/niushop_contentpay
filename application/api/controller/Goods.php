<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\api\controller;

use data\service\GoodsCategory;
use data\service\GoodsBrand;
use data\service\Goods as GoodsService;
use data\service\promotion\GoodsExpress;
use data\service\Address;
use data\service\Order;
use data\service\Platform;
class Goods extends BaseController
{
    function __construct()
    {
        parent::__construct();
    }
    /**
     * 获取首页商品分类楼层
     * @param unknown $shop_id  店铺id，默认0
     * @param unknown $num  查询商品数量
     */ 
    public function getGoodsCategoryBlockQuery(){
        $title = "获取首页商品分类楼层";
        $shop_id = request()->request('shop_id', 0);
        $num = request()->request('num', 4);
        $good_category = new GoodsCategory();
        $block_list = $good_category->getGoodsCategoryBlockQuery($shop_id, $num);
        return $this->outMessage($title, $block_list); 
    }
    /**
     * 获取商品品牌列表
     */
    public function getGoodsBrandList(){
        $title = "获取商品品牌列表";
        $page_index = request()->request("page_index", 1);
        $page_size = request()->request("page_size", 0);
        $condition = request()->request("condition", '');
        $order = request()->request('order', '');
        $goods_brand = new GoodsBrand();
        $list = $goods_brand->getGoodsBrandList($page_index, $page_size, $condition, $order);
        return $this->outMessage($title, $list);
    }
    /**
     * 商品详情
     *
     * @return Ambigous <\think\response\View, \think\response\$this, \think\response\View>
     */
    public function goodsDetail()
    {
        $title = "获取商品详情，需要必填参数goods_id";
        $goods_id = request()->request('goods_id', 0);
        if ($goods_id == 0) {
            return $this->outMessage($title, '','-50',"缺少必填参数goods_id");
        }
        $goods = new GoodsService();
        $goods_detail = $goods->getGoodsDetail($goods_id);
        if (empty($goods_detail)) {
              return $this->outMessage($title, '','-50',"没有获取到商品信息，请检验goods_id");
        }
        // 把属性值相同的合并
        $goods_attribute_list = $goods_detail['goods_attribute_list'];
        $goods_attribute_list_new = array();
        foreach ($goods_attribute_list as $item) {
            $attr_value_name = '';
            foreach ($goods_attribute_list as $key => $item_v) {
                if ($item_v['attr_value_id'] == $item['attr_value_id']) {
                    $attr_value_name .= $item_v['attr_value_name'] . ',';
                    unset($goods_attribute_list[$key]);
                }
            }
            if (! empty($attr_value_name)) {
                array_push($goods_attribute_list_new, array(
                'attr_value_id' => $item['attr_value_id'],
                'attr_value' => $item['attr_value'],
                'attr_value_name' => rtrim($attr_value_name, ',')
                ));
            }
        }
        $goods_detail['goods_attribute_list'] = $goods_attribute_list_new;
        $evaluates_count = $goods->getGoodsEvaluateCount($goods_id);
        $goods_detail['evaluates_count'] = $evaluates_count;
        return $this->outMessage($title, $goods_detail);
        // 查询点赞记录表，获取详情再判断当天该店铺下该商品该会员是否已点赞
      /*   $goods = new GoodsService();
        $shop_id = $this->instance_id;
        $uid = $this->uid;
        $click_detail = $goods->getGoodsSpotFabulous($shop_id, $uid, $goods_id);
        $this->assign('click_detail', $click_detail);
    
        // 当前用户是否收藏了该商品
        if (isset($this->uid)) {
            $member = new Member();
            $is_member_fav_goods = $member->getIsMemberFavorites($this->uid, $goods_id, 'goods');
        } */
    }
    /**
     * 根据定位查询当前商品的运费
     * 创建时间：2017年9月29日 15:12:55
     */
    public function getShippingFeeByAddressName()
    {
        $title = "根据地址查询当前商品的运费,传入地址信息名称";
        $goods_id = request()->request("goods_id", "");
        $province = request()->request("province", "");
        $city = request()->request("city", "");
        $express = "";
        if (! empty($goods_id)) {
                $goods_express = new GoodsExpress();
                $address = new Address();
                $province_id = $address->getProvinceId($province);
                $city_id = $address->getCityId($city);
                $district_id = $address->getCityFirstDistrict($city_id['city_id']);
                $express = $goods_express->getGoodsExpressTemplate($goods_id, $province_id['province_id'], $city_id['city_id'], $district_id);
            }
      
        return $this->outMessage($title, $express);
    }
    /**
     * 根据地址id查询当前商品的运费
     * 创建时间：2017年9月29日 15:12:55
     */
    public function getShippingFeeByAddressId()
    {
        $title = "根据地址查询当前商品的运费,传入地址信息id";
        $goods_id = request()->request("goods_id", "");
        $province = request()->request("province", "");
        $city = request()->request("city", "");
        $district = request()->request("district", '');
        $express = "";
        if (! empty($goods_id)) {
            $goods_express = new GoodsExpress();
            $express = $goods_express->getGoodsExpressTemplate($goods_id, $province, $city, $district);
        }
    
        return $this->outMessage($title, $express);
    }
    /**
     * 功能：商品评论
     * 创建人：李志伟
     * 创建时间：2017年2月23日11:12:57
     */
    public function getGoodsComments()
    {
        $title = "获取商品评论,传入商品参数商品id，comments_type:1,2,3";
        $comments_type = request()->request('comments_type', '');
        $condition['goods_id'] = request()->post('goods_id', '');
        if(empty($condition['goods_id']))
        {
            return $this->outMessage($title, "",'-50',"缺少必填参数goods_id");
        }
        if(empty($comments_type))
        {
           return $this->outMessage($title, "",'-50',"缺少必填参数comment_type");
        }
        $order = new Order();
        switch ($comments_type) {
            case 1:
                $condition['explain_type'] = 1;
                break;
            case 2:
                $condition['explain_type'] = 2;
                break;
            case 3:
                $condition['explain_type'] = 3;
                break;
            case 4:
                $condition['image|again_image'] = array(
                'NEQ',
                ''
                    );
                    break;
            default:
                return 
                $this->outMessage($title, "",'-50',"参数comment_type为1,2,3");
                
        }
        $condition['is_show'] = 1;
        $goodsEvaluationList = $order->getOrderEvaluateDataList(1, PAGESIZE, $condition, 'addtime desc');
        return $goodsEvaluationList;
    }
    /**
     * 返回商品数量和当前商品的限购
     *
     * @param unknown $goods_id
     */
    public function getGoodsCartInfo()
    {
        $title = "获取当前会员针对当前商品购物车数量以及限购数量";
        $goods_id = request()->request("goods_id", "");
        if(empty($goods_id))
        {
            return $this->outMessage($title, "",'-50',"缺少必填参数goods_id");
        }
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50',"缺少会员登录信息!");
        }
        $goods = new GoodsService();
        $cartlist = $goods->getCart($this->uid);
        $num = 0;
        foreach ($cartlist as $v) {
            if ($v["goods_id"] == $goods_id) {
                $num = $v["num"];
            }
        }
        $data = array(
            'cartcount'=> count($cartlist),
            'num'      => $num
        );
        return $this->outMessage($title, $data);
    }
    /**
     * 购物车
     */
    public function cart()
    {
        $title = "获取购物车信息,需要会员登录";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50',"缺少会员登录信息!");
        }
        $goods = new GoodsService();
        $cartlist = $goods->getCart($this->uid, 0);
        // 店铺，店铺中的商品
        $list = Array();
        for ($i = 0; $i < count($cartlist); $i ++) {
            $list[$cartlist[$i]["shop_id"] . ',' . $cartlist[$i]["shop_name"]][] = $cartlist[$i];
        }
        return $this->outMessage($title, $list);
    }
    
    /**
     * 添加购物车
     * 创建人：李广
     */
    public function addCart()
    {
        $title = "添加购物车,需要会员登录，以及cart_detail:注意是json序列";
        $cart_detail = request()->request('cart_detail', '');
        if (! empty($cart_detail)) {
            $cart_detail = json_decode($cart_detail, true);
        }else{
            return $this->outMessage($title, "",'-50',"缺少必填参数cart_detail!");
        }
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50',"缺少会员登录信息!");
        }
        $uid = $this->uid;
        $shop_id = $cart_detail["shop_id"];
        $shop_name = $cart_detail["shop_name"];
        $goods_id = $cart_detail['trueId'];
        $goods_name = $cart_detail['goods_name'];
        $num = $cart_detail['count'];
        $sku_id = $cart_detail['select_skuid'];
        $sku_name = $cart_detail['select_skuName'];
        $price = $cart_detail['price'];
        $cost_price = $cart_detail['cost_price'];
        $picture = $cart_detail['picture'];
        $goods = new GoodsService();
        $retval = $goods->addCart($uid, $shop_id, $shop_name, $goods_id, $goods_name, $sku_id, $sku_name, $price, $num, $picture, 0);
        return $this->outMessage($title, $retval);
    
    }
    
    /**
     * 购物车修改数量
     */
    public function cartAdjustNum()
    {
        $title = "修改购物车数量";
           $cart_id = request()->request('cartid', '');
           $num = request()->request('num', '');
           if(empty($cart_id))
           {
               return $this->outMessage($title, "",'-50',"缺少必填参数cart_id!");
               
           }
           if(empty($num))
           {
               return $this->outMessage($title, "",'-50',"缺少必填参数num!");
                
           }
           if(empty($this->uid))
           {
               return $this->outMessage($title, "",'-50',"缺少会员登录信息!");
           }
           $goods = new GoodsService();
           $retval = $goods->cartAdjustNum($cart_id, $num);
           return $this->outMessage($title, $retval);
    
    }
    
    /**
     * 购物车项目删除
     */
    public function cartDelete()
    {
        $title = "删除购物车, del_id:中间,隔开";
        $cart_id_array = request()->request('del_id', '');
        if(empty($cart_id_array))
        {
            return $this->outMessage($title, "",'-50',"缺少必填参数del_id!");
        }
      if(empty($this->uid))
           {
               return $this->outMessage($title, "",'-50',"缺少会员登录信息!");
           }
        $goods = new GoodsService();
        $retval = $goods->cartDelete($cart_id_array);
        return $this->outMessage($title, $retval);
     
    }
    
    /**
     * 平台商品分类列表
     */
    public function goodsClassificationList()
    {
        $title = "商品分类树，根据手机端商品分类设置显示";
        $goods_category = new GoodsCategory();
        $goods_category_list_1 = $goods_category->getGoodsCategoryList(1, 0, [
            "is_visible" => 1,
            "level" => 1
        ], 'sort');
    
        $goods_category_two_tree = $goods_category->getGoodsSecondCategoryTree();
        $data = array(
            'goods_category_list_1' => $goods_category_list_1,
            'goods_category_two_tree' => $goods_category_two_tree
        );
        return $this->outMessage($title, $data);
    }
    
    /**
     * 搜索商品显示
     */
    public function goodsSearchList()
    {
        $title = "商品列表查询";
        $sear_name = request()->request('search_name', '');
        $sear_type = request()->request('search_type', '');
        $order_state = request()->request('order_state', 'desc');
        $controlType = request()->request('control_type', '');
        $shop_id = request()->request('shop_id', 0);
        $page = request()->request("page", 1);
        $goods = new GoodsService();
        $condition['goods_name'] = [
            'like',
            '%' . $sear_name . '%'
        ];
        // 排序类型
        switch ($sear_type) {
            case 1:
                $order = 'sort desc'; // 时间
                break;
            case 2:
                $order = 'sales desc'; // 销售
                break;
            case 3:
                $order = 'promotion_price ' . $order_state; // 促销价格
                break;
            default:
                $order = 'sort desc, create_time desc';
                break;
        }
        switch ($controlType) {
            case 1:
                $condition = [
                'is_new' => 1
                ];
                break;
            case 2:
                $condition = [
                'is_hot' => 1
                ];
                break;
            case 3:
                $condition = [
                'is_recommend' => 1
                ];
                break;
            default:
                break;
        }
        if (! empty($shop_id)) {
            $condition['ng.shop_id'] = $shop_id;
        }
        $condition['state'] = 1;
        $search_good_list = $goods->getGoodsList($page, PAGESIZE, $condition, $order);
        return $this->outMessage($title, $search_good_list);
    }
    
    /**
     * 获取品牌专区广告位
     */
    public function getBrandAdvPosition(){
        $title = "品牌专区广告位";
        $platform = new Platform();
        // 品牌专区广告位
        $brand_adv = $platform->getPlatformAdvPositionDetail(1162);
        $this->outMessage($title, $brand_adv);
    }
    /**
     * 品牌专区
     */
    public function brandlist()
    {
        $title = "品牌专区商品列表";
        $goods = new GoodsService();
        $brand_id = request()->request("brand_id", "");
        $page_index = request()->request("page", 1);
        if (! empty($brand_id)) {
            $condition['ng.brand_id'] = $brand_id;
        }
        $condition['ng.state'] = 1;
        $list = $goods->getGoodsList($page_index, PAGESIZE, $condition, "ng.sort desc,ng.create_time desc");
        return $this->outMessage($title, $list);
        
    }
    
    /**
     * 商品列表
     */
    public function goodsList()
    {
        $title = "商品列表查询";
        $category_id = request()->request('category_id', ''); // 商品分类
        $brand_id = request()->request('brand_id', ''); // 品牌
        $order = request()->request('order', ''); // 商品排序分类
        $sort = request()->request('sort', 'desc'); // 商品排序分类
        $page = request()->request('page', 1);
        switch ($order) {
            case 1: // 销量
                $order = 'sales ';
                break;
            case 2: // 新品
                $order = 'is_new ';
                break;
            case 3: // 价钱
                $order = 'promotion_price ';
                break;
            default:
                $order = 'sale_date ';
                break;
        }

        $orderby = ""; // 排序方式
        if ($order != "") {
            $orderby = $order . " " . $sort;
        } else {
            $orderby = "ng.sort desc,ng.create_time desc";
        }

        $condition = array();
        if (! empty($category_id)) {
            $condition["ng.category_id"] = $category_id;
        } else
            if (! empty($brand_id)) {
                $condition["ng.brand_id"] = array(
                    "in",
                    $brand_id
                );
            }
        $condition['state'] = 1;
        $goods = new GoodsService();
        $goods_list = $goods->getGoodsList($page, PAGESIZE, $condition, $orderby);
        return $this->outMessage($title, $goods_list);
      
    }
    /**
     * 获取积分中心广告位
     */
    public function getintegralCenterAdvPosition(){
        $title = "积分中心广告位";
        $platform = new Platform();
        // 积分中心广告位
        $integral_adv = $platform->getPlatformAdvPositionDetail(1165);
       return $this->outMessage($title, $integral_adv);
    }
    /**
     * 积分中心
     *
     * @return \think\response\View
     */
    public function getIntegralCenterGoods()
    {
        $title = "获取积分中心商品,order_type:1.销量2.收藏3.点赞4.分享";
        // 积分中心商品
        $this->goods = new GoodsService();
        $order = "";
        // 排序
        $id = request()->request('order_type', '');
        if ($id) {
            if ($id == 1) {
                $order = "sales desc";
            } else
                if ($id == 2) {
                    $order = "collects desc";
                } else
                    if ($id == 3) {
                        $order = "evaluates desc";
                    } else
                        if ($id == 4) {
                            $order = "shares desc";
                        } else {
                            $id = 0;
                            $order = "";
                        }
        } else {
            $id = 0;
        }
    
        $page_index = request()->request('page', 1);
        $condition = array(
            "ng.state" => 1,
            "ng.point_exchange_type" => array(
                'NEQ',
                0
            )
        );
        $page_count = 25;
        $allGoods = $this->goods->getGoodsList($page_index, $page_count, $condition, $order);
        return $this->outMessage($title, $allGoods);
       
    }
    
    /**
     * 商品点赞赠送积分
     * @return Ambigous <multitype:unknown, multitype:unknown unknown string >
     */
    public function getClickPoint()
    {
        $title = "商品点赞获赠积分";
        $goods_id = request()->request('goods_id', '');
        if(empty($goods_id))
        {
            return $this->outMessage($title, '','-50',"缺少必填参数goods_id");
        }
        $shop_id = request()->request("shop_id",0);
        $uid = $this->uid;
        if(empty($uid))
        {
            return $this->outMessage($title, '','-50',"会员登录信息，清检验token");
        }
      
        $goods = new GoodsService();
        $retval = $goods->setGoodsSpotFabulous($shop_id, $uid, $goods_id);
        return $this->outMessage($title, $retval);
       
    }
    
    /**
     * 获取商品分类下的商品
     */
    public function getCategoryGoodsList()
    {
        $title = "获取商品分类下的商品列表";
        $page = request()->request("page", 1);
        $category_id = request()->request("category_id", 0);
        $goods = new GoodsService();
        if ($category_id == 0) {
            return $this->outMessage($title, '','-50',"缺少必填参数category_id");
        } else {
            $condition['ng.category_id'] = $category_id;
            $condition['ng.state'] = 1;
            $res = $goods->getGoodsList($page, PAGESIZE, $condition, "ng.sort desc,ng.create_time desc");
            return $this->outMessage($title, $res);
        }
       
   
    }
    
    /**
     * 查询商品的sku信息
     */
    public function getGoodsSkuInfo()
    {
        $title = "获取商品的sku信息";
        $goods_id = request()->request('goods_id', '');
        if(empty($goods_id))
        {
            return $this->outMessage($title, '','-50',"缺少必填参数goods_id");
        }
        $goods = new GoodsService();
        $data = $goods->getGoodsAttribute($goods_id);
        return $this->outMessage($title, $data);
    }
    
    
}
