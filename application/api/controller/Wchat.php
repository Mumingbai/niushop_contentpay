<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\api\controller;

use data\service\Config;
use data\extend\WchatOauth;
use data\service\GoodsCategory;
use data\service\Platform;
use data\service\GoodsBrand;
use data\service\Goods;
class Wchat extends BaseController
{
       
    public function getFansInfo(){
            $title = "获取微信粉丝信息，注意只能在微信浏览器";
            $url = request()->request("url", "");
            if(!empty($url))
            {
                $_SESSION['request_url'] = $url;
            }else{
                return $this->out_message($title, "","-50","未获取到要返回的url");
            }
            // 微信浏览器自动登录
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger')) {
                $config = new Config();
                $wchat_config = $config->getInstanceWchatConfig(0);
                if (empty($wchat_config['value']['appid'])) {
                    $out_message =  $this->outMessage($title, "","-50","当前系统未设置微信公众号！");
                   
                }
                $wchat_oauth = new WchatOauth();
                $domain_name = \think\Request::instance()->domain();
                if (! empty($_COOKIE[$domain_name . "member_access_token"])) {
                    $token = json_decode($_COOKIE[$domain_name . "member_access_token"], true);
                } else {
                    $token = $wchat_oauth->get_member_access_token();
                    if (! empty($token['access_token'])) {
                        setcookie($domain_name . "member_access_token", json_encode($token));
                        $info = $wchat_oauth->get_oauth_member_info($token);
                        $out_message = $this->outMessage($title, $info);
                    }
                }
            }else{
                $out_message = $this->outMessage($title, "","-50","请在微信浏览器登录");
            }
            if(!empty($out_message))
            {
                $this->redirect($_SESSION['request_url'].'?message='.json_encode($out_message));
            }
         
    }
    /**
     * 获取分享相关票据
     */
    public function getShareTicket()
    {
        $title = "获取微信票据";
        $url = request()->request("url", "");
        $request_url = request()->request("request_url", "");
        if(!empty($url))
        {
            $_SESSION['request_url'] = $request_url;
        }else{
                return $this->outMessage($title, "","-50","未获取到要返回的url");
            }
        
        $config = new Config();
        $auth_info = $config->getInstanceWchatConfig(0);
        // 获取票据
        if (! empty($auth_info['value']['appid'])) {
            // 针对单店版获取微信票据
            $wexin_auth = new WchatOauth();
            $signPackage['appId'] = $auth_info['value']['appid'];
            $signPackage['jsTimesTamp'] = time();
            $signPackage['jsNonceStr'] = $wexin_auth->get_nonce_str();
            $jsapi_ticket = $wexin_auth->jsapi_ticket();
            $signPackage['ticket'] = $jsapi_ticket;
            $Parameters = "jsapi_ticket=" . $signPackage['ticket'] . "&noncestr=" . $signPackage['jsNonceStr'] . "&timestamp=" . $signPackage['jsTimesTamp'] . "&url=" . $url;
            $signPackage['jsSignature'] = sha1($Parameters);
            $out_message = $this->outMessage($title, $signPackage);
        } else {
            $signPackage = array(
                'appId' => '',
                'jsTimesTamp' => '',
                'jsNonceStr' => '',
                'ticket' => '',
                'jsSignature' => ''
            );
            $out_message = $this->outMessage($title, $signPackage, '-9001', "当前微信没有配置!");
        }
        $this->redirect($_SESSION['request_url'].'?message='.json_encode($out_message));
    }
    
}
