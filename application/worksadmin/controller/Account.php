<?php
namespace app\worksadmin\controller;

use data\service\Order;
use data\worksservice\Statistics;
use data\worksservice\Order as orderService;
use think\helper\Time;

/**
 * 资产
 * @author lzw
 *
 */
class Account extends BaseController
{

    public function __construct(){
        parent::__construct();
        
    }
    
    public function index(){
        return view($this->style . "Account/index");
    }
    
    /**
     * 店铺销售概况
     *
     * @return Ambigous <\think\response\View, \think\response\$this, \think\response\View>
     */
    public function courseSalesAccount()
    {

        $statistics = new Statistics();
        $data =  $statistics->getOrderTimeByPrice();
        $this->assign('data',$data);
      
        return view($this->style . "Account/courseSalesAccount");
        
    }
    
    /**
     * 课程销售详情
     *
     * @return Ambigous <multitype:number , multitype:number unknown >
     */
    public function courseanalysis(){
        if (request()->isAjax()) {
            $statistics = new Statistics();
            $page_index = request()->post('page_index', 1);
            $page_size = request()->post('page_size', PAGESIZE);
            $course_title = request()->post("course_title", '');
            $condition = array();
            if ($course_title != '') {
                $condition = array(
                    "status" => [
                        'NEQ',
                        0
                    ],
                    "status" => [
                        'NEQ',
                        3
                    ]
                );
                $condition["course_title"] = array(
                    'like',
                    '%' . $course_title . '%'
                );
            }
            $list = $statistics->getcourseSalesList($page_index, $page_size, $condition, 'create_time desc');
            return $list;
        } else {
            return view($this->style . "Account/courseSalesList");
        }
    }
    
    /**
     * 课程运营报告
     */
    
    public function worksReport()
    {
        return view($this->style . "Account/worksReport");
    }
    
    
    
    /**
     * 课程销售排行
     */
    public function coursesalesrank(){
         $statistics = new Statistics();
         $course_list = $statistics->getCourseSaleRanking();
         $this->assign("course_list", $course_list);
         return view($this->style . "Account/coursesalesrank");
    }
    
    
    /**
     * 店铺下单量/下单金额图标数据
     *
     * @return Ambigous <multitype:, unknown>
     */
    public function getShopOrderChartCount()
    {
        $date = request()->post('date',1);
        $type = request()->post('type',1);
        $order = new Order();
        $data = array();
        if ($date == 1) {
            list ($start, $end) = Time::today();
            for ($i = 0; $i < 24; $i ++) {
                $date_start = date("Y-m-d H:i:s", $start + 3600 * $i);
                $date_end = date("Y-m-d H:i:s", $start + 3600 * ($i + 1));
                $condition = [
                    'create_time' => [
                        'between',
                        [
                            getTimeTurnTimeStamp($date_start),
                            getTimeTurnTimeStamp($date_end)
                        ]
                    ],
                    "order_status" => [
                        'NEQ',
                        0
                    ],
                    "order_status" => [
                        'NEQ',
                        5
                    ]
                ];
                $count = $this->getShopSaleData($condition, $type);
    
                $data[0][$i] = $i . ':00';
                $data[1][$i] = $count;
            }
        } else
            if ($date == 2) {
                list ($start, $end) = Time::yesterday();
                for ($j = 0; $j < 24; $j ++) {
                    $date_start = date("Y-m-d H:i:s", $start + 3600 * $j);
                    $date_end = date("Y-m-d H:i:s", $start + 3600 * ($j + 1));
                    $condition = [
                        'shop_id' => $this->instance_id,
                        'create_time' => [
                            'between',
                            [
                                getTimeTurnTimeStamp($date_start),
                                getTimeTurnTimeStamp($date_end)
                            ]
                        ],
                        "order_status" => [
                            'NEQ',
                            0
                        ],
                        "order_status" => [
                            'NEQ',
                            5
                        ]
                    ];
                    $count = $this->getShopSaleData($condition, $type);
                    $data[0][$j] = $j . ':00';
                    $data[1][$j] = $count;
                }
            } else
                if ($date == 3) {
                    $start = strtotime(date('Y-m-d 00:00:00', strtotime('last day this week + 1 day')));
                    for ($j = 0; $j < 7; $j ++) {
                        $date_start = date("Y-m-d H:i:s", $start + 86400 * $j);
                        $date_end = date("Y-m-d H:i:s", $start + 86400 * ($j + 1));
                        $condition = [
                            'shop_id' => $this->instance_id,
                            'create_time' => [
                                'between',
                                [
                                    getTimeTurnTimeStamp($date_start),
                                    getTimeTurnTimeStamp($date_end)
                                ]
                            ],
                            "order_status" => [
                                'NEQ',
                                0
                            ],
                            "order_status" => [
                                'NEQ',
                                5
                            ]
                        ];
                        $count = $this->getShopSaleData($condition, $type);
                        $data[0][$j] = '星期' . ($j + 1);
                        $data[1][$j] = $count;
                    }
                } else
                    if ($date == 4) {
                        list ($start, $end) = Time::month();
                        for ($j = 0; $j < ($end + 1 - $start) / 86400; $j ++) {
                            $date_start = date("Y-m-d H:i:s", $start + 86400 * $j);
                            $date_end = date("Y-m-d H:i:s", $start + 86400 * ($j + 1));
                            $condition = [
                                'shop_id' => $this->instance_id,
                                'create_time' => [
                                    'between',
                                    [
                                        getTimeTurnTimeStamp($date_start),
                                        getTimeTurnTimeStamp($date_end)
                                    ]
                                ],
                                "order_status" => [
                                    'NEQ',
                                    0
                                ],
                                "order_status" => [
                                    'NEQ',
                                    5
                                ]
                            ];
                            $count = $this->getShopSaleData($condition, $type);
                            $data[0][$j] = (1 + $j) . '日';
                            $data[1][$j] = $count;
                        }
                    }
                return $data;
    }
    
    /**
     * 查询一段时间内的总下单量及下单金额
     *
     * @return multitype:\app\admin\controller\Ambigous Ambigous <\app\admin\controller\Ambigous, number, \data\service\niushop\unknown, \data\service\niushop\Order\unknown, unknown>
     */
    public function getOrderShopSaleCount()
    {
        $date = request()->post('date',1);
        // 查询一段时间内的下单量及下单金额
        if ($date == 1) {
            list ($start, $end) = Time::today();
            $start_date = date("Y-m-d H:i:s", $start);
            $end_date = date("Y-m-d H:i:s", $end);
        } else
            if ($date == 3) {
                $start_date = date('Y-m-d 00:00:00', strtotime('last day this week + 1 day'));
                $end_date = date('Y-m-d 00:00:00', strtotime('last day this week +8 day'));
            } else
                if ($date == 4) {
                    list ($start, $end) = Time::month();
                    $start_date = date("Y-m-d H:i:s", $start);
                    $end_date = date("Y-m-d H:i:s", $end);
                }
            $condition = array();
            $condition["shop_id"] = $this->instance_id;
            $condition["shop_id"];
            $condition["create_time"] = [
                'between',
                [
                    getTimeTurnTimeStamp($start_date),
                    getTimeTurnTimeStamp($end_date)
                ]
            ];
            $count_money = $this->getShopSaleData($condition, 1);
            $count_num = $this->getShopSaleData($condition, 2);
            return array(
                "count_money" => $count_money,
                "count_num" => $count_num
            );
    }
    
    /**
     * 下单量/下单金额 数据
     *
     * @param unknown $condition
     * @param unknown $type
     * @return Ambigous <\data\service\niushop\Ambigous, \data\service\niushop\Order\unknown, number, unknown>
     */
    public function getShopSaleData($condition, $type)
    {
        $order = new orderService();
        if ($type == 1) {
            $count = $order->getShopSaleSum($condition);
            $count = (float) sprintf('%.2f', $count);
        } else {
            $count = $order->getShopSaleNumSum($condition);
        }
        return $count;
    }
    
    
    
}