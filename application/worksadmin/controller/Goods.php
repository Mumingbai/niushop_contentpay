<?php
/**
 * Cms.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\worksadmin\controller;

use data\worksservice\Goods as GoodsService;

/**
 * cms内容管理系统
 */
class Goods extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 商品列表
     */
    public function goodsList()
    {
        
        if (request()->isAjax()) {
            $goods = new GoodsService();
            $page_index = request()->post('page_index', 1);
            $page_size = request()->post('page_size', PAGESIZE);
            $result = $goods->getGoodsList($page_index, $page_size, '', '');
            return $result;
        } else {
            return view($this->style . 'Goods/goodsList');
        }
    }

    /**
     * 发布修改商品
     */
    public function ajaxAddUpdateCourse()
    {
        if (request()->isAjax()) {
            $goods_data = $_POST['goods_data'];
            $goods = new GoodsService();
            $result = $goods->addUpdateGoods($goods_data);
            return AjaxReturn($result);
        } else {
            $goods = new GoodsService();
            $goods_id = request()->get('goods_id','');
            $this->assign('goods_id',$goods_id);
            $goods_info = $goods->getGoodsInfo($goods_id);
            
            $goods_class_list = $goods->getGoodsClassList(1, 0, ['pid'=>0], 'sort asc');
            
            $this->assign('goods_info', $goods_info);
            $this->assign('goods_class_list', $goods_class_list['data']);
            
            return view($this->style . 'Goods/addUpdateGoods');
        }
    }
    
    /**
     * 删除商品
     */
    public function ajaxDelGoods(){
        if(request()->isAjax()){
            $goods_ids = request()->post('goods_ids', '');
            $goods = new GoodsService();
            $res = $goods->delGoods($goods_ids);
            return AjaxReturn($res);
        }
    }
    
    /**
     *商品分类列表
     */
    public function goodsClassList()
    {
        $goods = new GoodsService();
        $list = $goods->getGoodsClassList(1, 0, ['pid'=>0], 'sort asc');
        foreach($list['data'] as $key=>$val){
           $child_list = $goods->getGoodsClassList(1, 0, ['pid'=>$val['class_id']]);
           $val['child_list'] = $child_list;
        }
        
        $this->assign('list', $list['data']);
        return view($this->style. "Goods/goodsClassList");
    }
    
    /**
     * 添加修改商品分类  
     */
    public function ajaxAddUpdateGoodsClass()
    {
        
        $goods = new GoodsService();
        $data = request()->post();
        $goods_class_data = $data['goods_class_data'];
        
        $res = $goods->addUpdateGoodsClass($goods_class_data);
        return AjaxReturn($res); 
    }
    
    /**
     *删除商品分类
     */
    public function ajaxDelGoodsClass()
    {
        $goods_class_ids = request()->post('goods_class_ids');
        $goods_class_ids = rtrim($goods_class_ids, ',');
        
        $goods = new GoodsService();
        $res = $goods->delGoodsClass($goods_class_ids);
        return AjaxReturn($res);
    }
    
    /**
     * 查询单条数据
     */
    public function goodsClassInfo(){
        
        $class_id = request()->post('class_id');
        
        $goods = new GoodsService();
        $goods_class_info = $goods->getGoodsClassInfo($class_id);
        
        return $goods_class_info;
    }
    
    /**
     * 修改单个数据  
     */
    public function cmsfield(){
        
        $class_id = request()->post('fieldid');
        $fieldname = request()->post('fieldname');
        $fieldvalue = request()->post('fieldvalue');
        
        $goods_class_data['class_id'] = $class_id;
        $goods_class_data[$fieldname] = $fieldvalue;
        
        $goods = new GoodsService();
        $res = $goods->addUpdateGoodsClass($goods_class_data);
        
        return AjaxReturn($res);
    }
    
    

   
}