<?php
namespace app\worksadmin\controller;

use data\worksservice\Course as CourseService;
use data\worksservice\Works as worksservice;
class Course extends BaseController
{

    public function __construct(){
        parent::__construct();
        
    }
    
    /**
     * 录播管理
     */
    public function index(){
        
        if(request()->isAjax()){
        
            $course_service = new CourseService();
            $page_index = request()->post('page_index', 1);
            $page_size = request()->post('page_size', PAGESIZE);
            $keyword = request()->post('keyword','');
            $condition['course_title'] = ['like',"%".$keyword."%"]; 
        
            $course = $course_service->getCourseList($page_index, $page_size, $condition, 'create_time desc');
            return $course;
        }
        $this->assign('type', request()->get('type','0'));
        return view($this->style . "Course/index");
    }
    
    /**
     * 录播管理
     */
    public function courseList(){
    
        if(request()->isAjax()){
    
            $course_service = new CourseService();
            $page_index = request()->post('page_index', 1);
            $page_size = request()->post('page_size', PAGESIZE);
            $keyword = request()->post('keyword','');
            $condition['course_title'] = ['like',"%".$keyword."%"];
    
            $course = $course_service->getCourseList($page_index, $page_size, $condition, 'sort asc');
            return $course;
        }
        $this->assign('type', request()->get('type','0'));
        return view($this->style . "Course/courseList");
    }
    
    
    /**
     * 添加录播教程
     */
    public function addCourse(){
        
        $course_service = new CourseService();
        
        $subject_list = $course_service->getSubjectList(1,0,'','sort asc');
        $this->assign('subject_list', $subject_list['data']);

        return view($this->style . "Course/addUpdateCourse");
    }
    
    /**
     * 修改录播教程
     */
    public function updateCourse(){
        
        $course_id = request()->get('course_id', '');
        $course_service = new CourseService();
        
        $course_info = $course_service->getCourseInfo($course_id);
        $this->assign('course_info', $course_info);
        
        $subject_list = $course_service->getSubjectList(1,0,'','sort asc');
        $this->assign('subject_list', $subject_list['data']);
        
        return view($this->style . "Course/addUpdateCourse");
    }
    
    /**
     * ajax添加修改课程
     * @return multitype:unknown
     */
    public function ajaxAddUpdateCourse(){
        if(request()->isAjax()){
        
            $course_data = $_POST['course_data'];
            $teacher_ids = $_POST['teacher_ids'];
            $teacher_ids = rtrim($teacher_ids, ',');
            $course_service = new CourseService();
            $res = $course_service->addUpdateCourse($course_data, $teacher_ids);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 修改课程排序
     */
    public function ajaxCourseSort(){
        
        if(request()->isAjax()){
            
            $course_id = request()->post('course_id', '');
            $sort = request()->post('sort', '');
            
            $course_service = new CourseService();
            $res = $course_service->updateCourseSort($course_id, $sort);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 删除课程可多个删除,分隔
     */
    public function ajaxDelCourse(){
        
        if(request()->isAjax()){
        
            $course_ids = request()->post('course_ids', '');
            
            $course_service = new CourseService();
            $res = $course_service->delCourse($course_ids);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 教师中心
     */
    public function teacher(){
        
        if(request()->isAjax()){
            
            $course_service = new CourseService();
            $page_index = request()->post('page_index', 1);
            $page_size = request()->post('page_size', PAGESIZE);
            $keyword = request()->post('keyword', '');
            $condition['name'] = ['like','%'.$keyword.'%'];
            $teacher_list = $course_service->getTeacherList($page_index, $page_size, $condition, 'nt.sort asc');
            return $teacher_list; 
        }
        $this->assign('type', request()->get('type','0'));
        return view($this->style . "Course/teacher");
    }
    
    /**
     * 添加讲师
     */
    public function addTeacher(){
        
        $course_service = new CourseService();
        $subject_list = $course_service->getSubjectList(1,0,'','sort asc');
        $this->assign('subject_list', $subject_list['data']);
        
        return view($this->style . "Course/addUpdateTeacher");
    }
    
    /**
     * 修改讲师
     * @return \think\response\View
     */
    public function updateTeacher(){
        
        $id = request()->get('id','');
        
        //专业列表
        $course_service = new CourseService();
        $subject_list = $course_service->getSubjectList(1,0,'','sort asc');
        $this->assign('subject_list', $subject_list['data']);
        
        //讲师信息
        $teacher_info = $course_service->getTeacherInfo($id);
        $this->assign('teacher_info', $teacher_info);
        
        return view($this->style . "Course/addUpdateTeacher");
    }
    
    /**
     * ajax操作添加修改讲师
     */
    public function ajaxAddUpdateTeacher(){
        
        if(request()->isAjax()){
            
            $teacher_data = $_POST['teacher_data'];
             
            $course_service = new CourseService();
            $res = $course_service->addUpdateTeacher($teacher_data);
            return AjaxReturn($res);
        }
    }
    
    /**
     * ajax删除讲师，分隔删除多个
     */
    public function ajaxDelTeachers(){
        
        if(request()->isAjax()){
        
            $teacher_ids = $_POST['teacher_ids'];
            $course_service = new CourseService();
            $res = $course_service->delTeacher($teacher_ids);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 专业列表
     */
    public function subject(){
        
        $course_service = new CourseService();
        $subject_list = $course_service->getSubjectList(1,0,'','sort asc');
        
        $data = $subject_list['data'];
        
        //若有更多级 建议使用无限极分类
        foreach($data as $key=>$val){
            //找出顶级分类
            if($val['parent_id']==0){
                $list[$key] = $val;
                $child_list = [];
                foreach($data as $ke=>$va){
                    //找出下级分类
                    if($va['parent_id']==$val['subject_id']){
                        $child_list[]=$va;
                        $list[$key]['child_list']=$child_list;
                    }
                }
                //如果没有下级分类 赋值为空数组
                if($child_list == []){
                    $list[$key]['child_list']= [];
                }
            }
        }
        
        $this->assign('list', $list);
        //var_dump($list);die;
        return view($this->style . "Course/subject");
    }
    
    /**
     * 添加修改专业/分类
     */
    public function addSubject(){
        
        $course_service = new CourseService();
        
        //顶级专业列表
        $subject_list = $course_service->getSubjectList(1,0, ['parent_id'=>0]);
        $this->assign('subject_list', $subject_list['data']);
        
        return view($this->style . "Course/addUpdateSubject");
    }
    
    /**
     * 修改专业/分类
     */
    public function updateSubject(){
        
        $subject_id = request()->get('subject_id', '');
        $course_service = new CourseService();
        
        //专业详情信息
        $subject_info = $course_service->getSubjectInfo($subject_id);
        $this->assign('subject_info', $subject_info);
        
        //顶级专业列表
        $subject_list = $course_service->getSubjectList(1,0, ['parent_id'=>0]);
        $this->assign('subject_list', $subject_list['data']);
        
        return view($this->style . "Course/addUpdateSubject");
    }
    
    /**
     * ajax执行添加修改专业
     */
    public function ajaxAddUpdateSubject(){
        
        if(request()->isAjax()){
            
            $subject_data = $_POST['subject_data'];
            $course_service = new CourseService();
            $res = $course_service->addUpdateSubject($subject_data);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 删除专业/分类
     */
    public function ajaxDelSubject(){
        
        if(request()->isAjax()){
        
            $subject_id = request()->post('subject_id', '');
            $course_service = new CourseService();
            $res = $course_service->delSubject($subject_id);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 课程章节管理
     */
    public function courseKpoint(){
        
        $course_id = request()->get('course_id', '');
        $open_kpoint_ids = request()->get('open_kpoint_ids', '');
        $this->assign('course_id', $course_id);
        $this->assign('open_kpoint_ids', $open_kpoint_ids);
        
        $course_service = new CourseService();
        $course_kpoint_list = $course_service->getCourseKpointList($course_id);
        
        
        $open_kpoint_id_array = rtrim($open_kpoint_id_array);
        $open_kpoint_id_array = explode(',', $open_kpoint_ids);
        foreach ($course_kpoint_list as $item){
            $item['is_open'] = 0;
            foreach($open_kpoint_id_array as $open_kpoint_id){
                if($open_kpoint_id == $item['kpoint_id']){
                    $item['is_open'] = 1;
                }
            }
        }
   
        /* dump($course_kpoint_list);
        exit(); */
        $this->assign('course_kpoint_list', $course_kpoint_list);
        
        return view($this->style . "Course/courseKpoint");
    }
    
    /**
     * 课程章节添加
     */
    public function addCourseKpoint(){
        
        $kpoint_id = request()->get('kpoint_id');
        $course_service = new CourseService();
        $parent_kpoint_info = $course_service->getCourseKpointInfo($kpoint_id);
        $course_info = $course_service->getCourseInfo($parent_kpoint_info['course_id']);
        
        $this->assign('parent_kpoint_info',$parent_kpoint_info);
        $this->assign('course_info',$course_info);
        
        $works_service = new worksservice();
        $db_class_list = $works_service->getDbClassList($page_index, $page_size, '', 'sort asc');
        $this->assign('db_class_list',$db_class_list['data']);
        
        return view($this->style . "Course/addUpdateCourseKpoint");
    }
    
    /**
     * 课程章节修改
     */
    public function updateCourseKpoint(){
        
        $kpoint_id = request()->get('kpoint_id','');
        $course_service = new CourseService();
        $course_kpoint_info = $course_service->getCourseKpointInfo($kpoint_id);
        $parent_kpoint_info = $course_service->getCourseKpointInfo($course_kpoint_info['parent_id']);
        $course_info = $course_service->getCourseInfo($parent_kpoint_info['course_id']);
        
        
        $this->assign('course_kpoint_info', $course_kpoint_info);
        $this->assign('parent_kpoint_info',$parent_kpoint_info);
        $this->assign('course_info',$course_info);
        //dump($course_kpoint_info);
        
        
        $works_service = new worksservice();
        $db_class_list = $works_service->getDbClassList($page_index, $page_size, '', 'sort asc');
        
        $works_info = $works_service->getDbWorksInfo('',['works_code'=>$course_kpoint_info['works_code']]);
        /* dump($works_info);
        exit; */
        
        $this->assign('db_class_list',$db_class_list['data']);
        $this->assign('works_info',$works_info);
        
        return view($this->style . "Course/addUpdateCourseKpoint");
    }
    
    /**
     * 添加修改章节
     * @return \think\response\View
     */
    public function addUpdateCourseKpoint(){
        
        $this->assign('get_data', $_GET);
        
        $kpoint_id = request()->get('kpoint_id','');
        $course_service = new CourseService();
        $course_kpoint_info = $course_service->getCourseKpointInfo($kpoint_id);
        $this->assign('course_kpoint_info', $course_kpoint_info);
        
        return view($this->style . "Course/addUpdateCourseKpoint");
    }
    
    /**
     * 删除章节目录
     */
    public function ajaxDelCourseKpoint(){
        
        if(request()->isAjax()){
            
            $kpoint_id = request()->post('kpoint_id', '');
            $course_service = new CourseService();
            $res = $course_service->delCourseKpoint($kpoint_id);
            return AjaxReturn($res);
        }
        
    }
    
    /**
     * ajax添加修改章节
     */
    public function ajaxAddUpdateCourseKpoint(){
        
        if(request()->isAjax()){
            
            $course_kpoint_data = $_POST['course_kpoint_data'];
     
            $course_service = new CourseService();
            $res = $course_service->addUpdateCourseKpoint($course_kpoint_data);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 套餐管理
     */
    public function package(){
        
        if(request()->isAjax()){
            
            $page_index = request()->post('page_index', 1);
            $page_size = request()->post('page_size', PAGESIZE);
            
            $course_service = new CourseService();
            $package_list = $course_service->getPackageList($page_index, $page_size, '', 'sort asc');
            return $package_list;
        }
          $this->assign('type', request()->get('type','0')); 
        return view($this->style . "Course/package");
    }
    
    /**
     * 添加套餐
     */
    public function addPackage(){
        
        return view($this->style . "Course/addUpdatePackage");
    }
    
    /**
     * 修改套餐
     */
    public function updatePackage(){
        
        $package_id = request()->get('package_id', '');
       
        $course_service = new CourseService();
        $package_info = $course_service->getPackageInfo($package_id);
        $this->assign('package_info', $package_info);
        
        $package_course_list = $course_service->getPackageCourseList($package_id,'');
        $this->assign('package_course_list',$package_course_list);
              
        return view($this->style . "Course/addUpdatePackage");
    }
    
    /**
     * 添加修改套餐
     */
    public function ajaxAddUpdatePackage(){
        
        if(request()->isAjax()){
            
            $package_data = $_POST['package_data'];
            $package_id = $package_data['package_id'];
            $course_ids = rtrim($package_data['course_ids'],',');
            unset($package_data['course_ids']);
            
            $course_service = new CourseService();
            
            $res = $course_service->addUpdatePackage($package_data);
            if($package_data['package_id']){
                $del_course_res = $course_service->delPackageCourse('',['package_id'=>$package_id]);
                $add_course_res = $course_service->addBatchPackageCourse($package_id, $course_ids);
            }else{
                $add_course_res = $course_service->addBatchPackageCourse($res, $course_ids);
            }
            
            return AjaxReturn($res);
        }
    }
    
    /**
     * 删除套餐
     */
    public function ajaxDelPackage(){
        
        if(request()->isAjax()){
            
            $package_ids = request()->post('package_ids');
            $course_service = new CourseService();
            $res = $course_service->delPackage($package_ids);
            return AjaxReturn($res);
        }
    }
    
    
    /**
     * 添加套餐课程
     */
    public function packageCourse(){
        if(request()->isAjax()){
           
            $package_id = request()->post('package_id', '');    
            $course_service = new CourseService();
            $condition='';
            $package_list = $course_service->getPackageCourseList($package_id,$condition);
            return $package_list;
        }
        $package_id = request()->get('package_id','');
        $this->assign('package_id',$package_id);
        return view($this->style . "Course/packagecourse");
    }
    
    /**
     * 删除套餐课程
     */
    public function ajaxDelPackageCourse(){
       
        
            if(request()->isAjax()){
        
                $pc_id = request()->post('pc_id');
                $course_service = new CourseService();
                $res = $course_service->delPackageCourse($pc_id);
                return AjaxReturn($res);
            }
        
    }
    
    
    
    /**
     * 讲师列表
     * @return \think\response\View
     */
    public function teacherList(){
        return view($this->style . "Course/teacherList");
    }
    
    /**
     * ajax讲师列表
     */
    public function ajaxTeacherList(){
        
        if(request()->isAjax()){
            
            $course_service = new CourseService();
            $teacher_ids = request()->post('teacher_ids', '');
            $teacher_ids = rtrim($teacher_ids, ',');
            $teacher_list = $course_service->getTeacherList(1,0,['id'=>array('in',$teacher_ids)]);
            return $teacher_list;
        }
        
    }
    /**
     * ajax课程列表
     */
    public function ajaxCourseList(){
    
        if(request()->isAjax()){
    
            $course_service = new CourseService();
            $course_ids = request()->post('course_ids', '');
            $course_ids = rtrim($course_ids, ',');
            $course_list = $course_service->getCourseList(1,0,['course_id'=>array('in',$course_ids)]);
            return $course_list;
        }
    
    }
    
    public function addPackgeCourse(){
        if(request()->isAjax()){
        
            $course_service = new CourseService();
            $course_ids = request()->post('course_ids', '');
            $package_id = request()->post('package_id','');
            $course_ids = rtrim($course_ids, ',');
            $retval = $course_service->addBatchPackageCourse($package_id, $course_ids);
            return AjaxReturn($retval);
        }
    }
    

}