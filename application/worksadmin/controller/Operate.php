<?php
namespace app\worksadmin\controller;
use data\worksservice\Operate as OperateService;
/**
 * 运营
 * @author lzw
 *
 */
class Operate extends BaseController
{
    
    private $operate_service = null;
    public function __construct(){
        parent::__construct();
        
        $this->operate_service = new OperateService();
    }
    public function index(){
    }
    
    /**
     * 优惠劵列表
     */
    public function couponList(){
        
        if(request()->isAjax()){
        
            $page_index = request()->post('page_index', 1);
            $page_size = request()->post('page_size', PAGESIZE);
        
            $coupon_list = $this->operate_service->getCouponList($page_index, $page_size);
            return $coupon_list;
        }
        return view($this->style . "Operate/couponList");
    }
    
    /**
     * 添加优惠劵
     */
    public function addCoupon(){
        
        $coupon_info = array(
            'type_value' => 0.00,
            'condition_money' => 0.00,
            'total_num' => 0,
            'each_max_num' => 0
        );
        $this->assign('coupon_info', $coupon_info);
        return view($this->style . "Operate/addUpdateCoupon");
    }
    
    /**
     * 修改优惠劵
     * @return \think\response\View
     */
    public function updateCoupon(){
        
        $vcoupon_id = request()->get('vcoupon_id', '');
        $coupon_info = $this->operate_service->getCouponInfo($vcoupon_id);
        $this->assign('coupon_info', $coupon_info);
        return view($this->style . "Operate/addUpdateCoupon");
    }
    
    /**
     * 添加修改优惠劵
     */
    public function ajaxAddUpdateCoupon(){
        
        if(request()->isAjax()){
            
            $coupon_data = $_POST['coupon_data'];
            $res = $this->operate_service->addUpdateCoupon($coupon_data);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 删除优惠劵
     */
    public function delCoupon(){
        
        if(request()->isAjax()){
        
            $vcoupon_ids = request()->post('vcoupon_ids', '');
            $res = $this->operate_service->delCoupon($vcoupon_ids);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 停止开始优惠劵活动
     */
    public function stopItCoupon(){
        
        if(request()->isAjax()){
        
            $vcoupon_id = request()->post('vcoupon_id', '');
            $status = request()->post('status', '');
            $res = $this->operate_service->updateCouponStatus($vcoupon_id, $status);
            return AjaxReturn($res);
        }
    }
    
    
}