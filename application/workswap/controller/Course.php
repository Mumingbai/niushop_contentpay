<?php
namespace app\workswap\controller;
use data\worksservice\Course as CourseService;
use data\service\Member as MemberService;
use data\worksservice\Vmember;
use data\worksservice\VmemberAccount;
class Course extends BaseController
{
    private $course_service = null;
    public function __construct(){
        parent::__construct();
        $this->course_service = new CourseService();
    }
    
    /**
     * 课程列表页
     */
    public function index(){
        
        $condition['is_visible'] = 1;
        $condition['parent_id'] = 0;
        $subject_list = $this->course_service->getSubjectList(1,0, $condition,'sort asc');
        
        foreach($subject_list['data'] as $key=>$val){
            $condition['parent_id'] = $val['subject_id'];
            $child_list = $this->course_service->getSubjectList(1,0, $condition,'sort asc');
            $val['child_list'] = $child_list['data'];
        }
        $this->assign('subject_list', $subject_list['data']); 
        
        $keyword = request()->post('keyword');
        $this->assign('keyword',$keyword);
        
        return view($this->style . 'Course/index');
    }
    
    /**
     * 课程列表具体分页数据
     */
    public function controlCourseList1(){
    
        $condition['status'] = ['in','1,2'];
        $subject_id_1 = request()->post('subject_id_1');
        $subject_id_2 = request()->post('subject_id_2');
        $order = request()->post('order');
        $sort = request()->post('sort');
        $keyword = request()->post('keyword');
        $page = request()->post('page');
    
        if(!empty($subject_id_1)) $condition['subject_id_1'] = $subject_id_1;
        if(!empty($subject_id_2)) $condition['subject_id_2'] = $subject_id_2;
        if(empty($order)) $order = "sort";
        if(empty($sort)) $sort = "asc";
        if(!empty($keyword)) $condition['course_title'] = ['like','%'.$keyword.'%'];
        
        $field = 'cover_img,course_title,basics_buy,buy_count,current_price,course_id,status,subject_id_1,subject_id_2,lose_type,lose_time,end_time,create_time,modify_time';
        
        $course_list = $this->course_service->getCourseList($page, 10, $condition, "$order $sort", $field);
    
        if(!empty($course_list['data'])){
            return $course_list['data'];
        }else{
            return 0;
        }
    }
    
    /**
     * 视频详情页
     */
    public function courseInfo(){
        
        // 获取课程详情
        $course_data = new CourseService();
        $course_id = request()->get("course_id",'');
        $this->assign('course_id', $course_id);
        
        $course = $course_data->getCourseInfo($course_id);
        $this->assign("course",$course);
         
        //获取讲师相资料
        foreach($course['course_teacher_list'] as $key=>$val)
        {
            $id_list[]=$val['teacher_id'];
        }
        $id_list=join(',',$id_list);
        $condition['id']=['in',$id_list];
        $teacher_list=$course_data->getTeacherList(1,0,$condition);
        $this->assign('teacher_list',$teacher_list['data']);
         
        // 获取课程章节列表
        $kpoint_list = $course_data->getCourseKpointList($course_id);
  
        $this->assign('kpoint_list',$kpoint_list);
        
         //获取该课程是否被收藏
         $course_favourse = new VmemberAccount();
         $relation_id = $course_id;
         $is_collection = $course_favourse->isCourseCollection($relation_id);
         $this->assign('is_collection',$is_collection);
         
         // 评论列表
         $account = new VmemberAccount;
         $condition_e['relation_id'] = $course_id;
         $condition_e['relation_type'] = 1;
         $condition_e['is_show'] = 1;
         $condition_e['reply_evaluate'] = 0;
          
         $data = $account->getMemberEvaluateList(1,0,$condition_e,'evaluate_id desc');
          
         $evaluate_list = $data['data'];
          
         foreach($evaluate_list as $key=>$val){
             $condition_r['relation_id'] = $course_id;
             $condition_r['relation_type'] = 1;
             $condition_r['is_show'] = 1;
             $condition_r['reply_evaluate'] = $val['evaluate_id'];
             $val['reply_list'] = $account-> getMemberEvaluateList(1,0,$condition_r,"evaluate_id desc")['data'];
             
         }
          
         $this->assign('evaluate_list',$evaluate_list);
         
         //用户信息
         $member = new MemberService();
         $member_info = $member->getMemberDetail($this->instance_id);
         if(!$member_info){
             $member_info = [];
         }
         $this->assign('member_info',$member_info);
         
         //检测会员登录 对课程的拥有信息 及其他做判断所需数据
         $kpoint_id = request()->get('kpoint_id');
         $kpoint_count = count($kpoint_list); 
         $this->isHaveWorks($kpoint_count, $kpoint_list, $kpoint_id);
         
         //查看是否属于某一套餐及获取该套餐的详情
         $is_belong_package = $course_data->getPackageCourseList('',['course_id' => $course_id]);
         if($is_belong_package)
         {
             foreach($is_belong_package as $key=>$val)
             {
                 $package_info = $course_data->getPackageInfo($val['package_id']);
                 $package_course_list = $course_data->getPackageCourseList($val['package_id']);
                 $package_list[$key][0] = $package_info;
                 $package_list[$key][1] = $package_course_list;
             }
              
         }
          
         $this->assign('package_list' , $package_list);
         
         //获取推荐课程列表
         $course_list = $course_data->getCourseList(1,3,['status'=>['in','1,2']],'modify_time desc');
         $this->assign("course_list",$course_list['data']);
         
         
         
         
        return view($this->style.'Course/courseInfo');
    }
    /**
     * 添加评论
     *
     */
    public function addComment(){
        $evaluate_text = request()->post('content');
        $relation_id = request()->post('course_id');
        $relation_type = request()->post('evaluate_type');
        $reply_evaluate = request()->post('reply_id');
    
        $account = new VmemberAccount();
        $res = $account->addMemberEvaluate($relation_type, $relation_id, $evaluate_text, $reply_evaluate);
        $time = date('Y-m-d H:i');
        $data = [$res,$time];
        return json_encode($data);
         
    }
    
    /**
     * 是否拥有课程
     */
    public function isHaveWorks($kpoint_count, $kpoint_list, $kpoint_id){

        $member = new MemberService();
        $member_info = $member->getMemberDetail($this->instance_id);
        
        $is_login = 0;
        
        //会员是否登录
        if(!empty($member_info)){
            $is_login = 1;
        }
        $this->assign('is_login', $is_login);
        $course_id = request()->get('course_id', '');
        $vmember_service = new Vmember();
        
        // 查询会员是否已购买课程或课程章节
        $vmember_service = new Vmember();
         
        $condition_h['uid'] = $this->uid;
        $condition_h['relation_type'] = 1;
        $condition_h['relation_id'] = $course_id;
        $have_info = $vmember_service->getVmemberHaveInfo('' , $condition_h);
        
        // 初始化相关数据
        $kpoint_arr = [];//已购买章节数组
        $is_have_course = 0;//是否已拥有整个课程的
        $action = 0;// 控制评论与回复
        $free_count = 0;//免费章节数统计
        $free_arr = [];
        $start_key = 0;
        
        // 计算已购买章节的个数
        if(!empty($have_info['kpoint_id'])) $kpoint_arr = explode(',', $have_info['kpoint_id']);
        $have_count = count($kpoint_arr);
        
        // 计算免费章节的个数
        foreach($kpoint_list as $key=>$val){
             
            if($val['kpoint_price'] == 0.00){
                $free_count++;
                $free_arr[] = $val['kpoint_id'];
            } 
        }
        
        if(!empty($have_info)){
            
            if(empty($have_info['kpoint_id'])){
                
                $is_have_course = 1;
            }else{
                // 如果已购买和免费的章节之和等于课程章节的总数 则表示客户已拥有了整个课程 将立即购买改为立即观看
                if($have_count + $free_count == $kpoint_count) $is_have_course = 1;
            }
        }
        
           
        // 如果免费章节数或者已购买章节数不为零 则可以评论和回复和观看
        if($free_count > 0 || $have_count > 0) $action = 1;
        
        $this->assign('action', $action);
        $this->assign("is_have_course", $is_have_course);
        $this->assign("kpoint_arr", $kpoint_arr);
        
        // 分情况构造播放信息
        $view_info = [];
        $account = new VmemberAccount();
        $course_data = new CourseService();
        
        
        if(!empty($kpoint_id)){
             
            // 从观看记录调转过来 则找到观看记录
            $study_info = $account->getVmemberStudyHistoryInfo($kpoint_id);
            
            $view_info['works_url'] = $course_data->getWorksUrl($kpoint_id)['works_url'];
            
            if(!empty($study_info)){
                
                $view_info['play_stop_time'] = $study_info['play_stop_time'];
                $view_info['parent_kpoint_id'] = $study_info['parent_kpoint_id'];
                $view_info['kpoint_id'] = $kpoint_id;
            }else{
                
                $kpoint_info = $course_data->getCourseKpointInfo($kpoint_id);
                
                $view_info['play_stop_time'] = 0;
                $view_info['parent_kpoint_id'] = $kpoint_info['parent_id'];
                $view_info['kpoint_id'] = $kpoint_id;
            }
            
            
        }else{
            $study_list = $account->getVmemberStudyHistoryList(1,1,['nms.course_id'=>$course_id],'end_play_time desc');
            
            if(!empty($study_list['data'])){
            
                // 从列表页调转过来 并且之前有过观看记录 同样找到观看记录
                $view_info['works_url'] = $course_data->getWorksUrl($study_list['data'][0]['kpoint_id'])['works_url'];
                $view_info['play_stop_time'] = $study_list['data'][0]['play_stop_time'];
                $view_info['parent_kpoint_id'] = $study_list['data'][0]['parent_kpoint_id'];
                $view_info['kpoint_id'] = $study_list['data'][0]['kpoint_id'];
            
            }else{
            
                // 从列表页跳转过来 但是之前没有观看记录 从免费的或是已购买的章节中找到最前面的一章
                foreach($kpoint_list as $key=>$val){
            
                    $id = $val['kpoint_id'];
                    if(in_array($id, $free_arr) || in_array($id, $kpoint_arr)){
                        $view_info['works_url'] = $val['children_list'][0]['works_url']['works_url'];
                        $view_info['parent_kpoint_id'] = $val['children_list'][0]['parent_id'];
                        $view_info['kpoint_id'] = $val['children_list'][0]['kpoint_id'];
                        break;
                    }
                }
            
                if(empty($view_info['works_url'])){
                    $view_info['works_url'] = '';
                    $view_info['parent_kpoint_id'] = 0;
                    $view_info['kpoint_id'] = 0;
                }
                $view_info['play_stop_time'] = 0;
            
            }
            
            
        } 
            
        $this->assign('view_info', $view_info);
        
        
    }
    /**
     * 站内搜索
     */
    public function SiteSearch(){
        if(request()->isAjax()){
            $course_data = new CourseService();
            $search = request()->post('search','');
            $condition['course_title'] = [
                'like',
                '%' . $search . '%'
            ];
            $course_list = $course_data->getCourseList(1, 0, $condition, 'sort asc');
            return $course_list;
        }
        return view($this->style.'Course/SiteSearch');
    }
    
    
    /**
     * 获取课程目录的播放地址
     */
    public function getKpointWorksUrl(){
        
        if(request()->isAjax()){
            $kpoint_id = request()->post('kpoint_id', '');
            $course_service = new CourseService();
            $works_result = $course_service->getWorksUrl($kpoint_id);
            return $works_result;
            
        }
   }
}