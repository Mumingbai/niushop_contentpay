<?php
namespace app\workswap\controller;
use data\worksservice\Course;
use data\service\Member as MemberService;
use data\worksservice\VmemberAccount;
use data\worksservice\Vmember;
class Works extends BaseController
{

    public function __construct(){
        parent::__construct();
        
        
        $member = new MemberService();
        $member_info = $member->getMemberDetail($this->instance_id);
        
        //会员是否登录
        if(empty($member_info)){
            $redirect = __URL(__URL__ . "/index");
            $this->redirect($redirect);
        }
        
        $course_id = request()->get('course_id', '');
        $vmember_service = new Vmember();
        //会员是否拥有该课程
        $is_have_course = $vmember_service->getVmemberHaveIsCourse($course_id);
        if(empty($is_have_course)) {
            
            $redirect = __URL(__URL__ . "/index");
            $this->redirect($redirect);
        }
        
        if (! empty($member_info['user_info']['user_headimg'])) {
            $member_info['member_img'] = $member_info['user_info']['user_headimg'];
        } else {
            $member_info['member_img'] = '0';
        }
        $this->assign('member_info', $member_info);
    }
    
    /**
     * 作品展示
     * @return \think\response\View
     */
    public function index(){
        
        $kpoint_id = request()->get('kpoint_id', '');
        
        $course_id = request()->get('course_id', '');
        
        $course_service = new Course();  
        $vmember_account = new VmemberAccount();
        
        //如果只传入课程id未传入目录id就按上次最新的播放地址走，如果未播放过取第一章第一节
//         if(empty($kpoint_id)){
            
            
//         }
        
        //课程信息
        $course_info = $course_service->getCourseInfo($course_id);
        $this->assign('course_info', $course_info);
        
        //获取该课程目录
        $course_kpoint = $course_service->getCourseKpointList($course_id);
     
        $this->assign('course_kpoint', $course_kpoint);
        
        //作品播放地址
        $works_result = $course_service->getWorksUrl($kpoint_id);
        $this->assign('works_url', $works_result['works_url']);
        
        //获取上次播放停止时间
        
        $study_history = $vmember_account->getVmemberStudyHistoryInfo($kpoint_id,'play_stop_time');
        $this->assign('play_stop_time', empty($study_history) ? '0' : $study_history['play_stop_time']);
        
        $this->assign('kpoint_id', $kpoint_id);
        return view($this->style . 'Works/index');
    }
    
    /**
     * 会员学习记录修改
     */
    public function ajaxMemberStudyRecord(){
        
        if(request()->isAjax()){
            
            $kpoint_id = request()->post('kpoint_id', '');
            $play_stop_time = request()->post('play_stop_time', '0');
            
            $vmember_account_service = new VmemberAccount();
            $res = $vmember_account_service->addUpdateVmemberStudyHistory($kpoint_id, $play_stop_time);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 会员添加修改学习笔记
     */
    public function ajaxMemberStudyNote(){
        
        if(request()->isAjax()){
        
            $kpoint_id = request()->post('kpoint_id', '');
            $note_text = request()->post('note_text', '');
            $note_id = request()->post('note_id', '');
            $works_play_time = request()->post('works_play_time', 0);
            
            $vmember_account_service = new VmemberAccount();
            $res = $vmember_account_service->addUpdateMemberStudyNote($note_id, $kpoint_id, $note_text, $works_play_time);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 学习笔记列表
     */
    public function MemberStudyNote(){
        
        $kpoint_id = request()->post('kpoint_id', '');
        $vmember_account_service = new VmemberAccount();
        $study_note_list = $vmember_account_service->getMemberKpointStudyNoteList($kpoint_id);
        $this->assign('study_note_list', $study_note_list);
        return view($this->style . 'Works/controlStudyNote');
    }
    
    /**
     * 删除学习笔记
     */
    public function ajaxDelMemberStudyNote(){
        
        if(request()->isAjax()){
            
            $note_id = request()->post('note_id', '');
            
            $vmember_account_service = new VmemberAccount();
            $res = $vmember_account_service->delMemberStudyNote($note_id);
            return AjaxReturn($res);
        }
    }

    /**
     * 课程添加讨论
     */
    public function ajaxAddMemberCourseDiscuss(){
    
        if(request()->isAjax()){
            
            $course_id = request()->post('course_id', '');
            $kpoint_id = request()->post('kpoint_id', '');
            $discuss_text = request()->post('discuss_text', '');
            
            $vmember_account_service = new VmemberAccount();
            $res = $vmember_account_service->addMemberCourseDiscuss($course_id, $kpoint_id, $discuss_text);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 课程讨论列表
     */
    public function MemberCourseDiscuss(){
        
        $course_id = request()->post('course_id', '');
        $vmember_account_service = new VmemberAccount();
        $course_discuss_list = $vmember_account_service->getMemberCourseDiscussList($course_id);
        $this->assign('course_discuss_list', $course_discuss_list);
        return view($this->style . 'Works/controlCourseDiscuss');
    }
    
    /**
     * 返回播放地址
     */
    public function getWorskUrl(){

        $kpoint_id = request()->get('kpoint_id','');
        
        $course_service = new Course();
        $result = $course_service->getWorksUrl($kpoint_id);
      
        if($result['code'] > 0){
           
            return $this->http($result['works_url'], '');
        }else{
            return AjaxReturn($result['code']);
        }
    }
  
    /**
     * 发送HTTP请求方法，目前只支持CURL发送请求
     * @param  string $url    请求URL
     * @param  array  $params 请求参数
     * @param  string $method 请求方法GET/POST
     * @return array  $data   响应数据
     */
    protected function http($url, $params, $method = 'GET', $header = array(), $multi = false){
        $opts = array(
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER     => $header
        );
    
        /* 根据请求类型设置特定参数 */
        switch(strtoupper($method)){
            case 'GET':
                if(empty($params)){
                    $opts[CURLOPT_URL] = $url;
                }else {
                    $opts[CURLOPT_URL] = $url . '?' . http_build_query($params);
                }
    
                break;
            case 'POST':
                //判断是否传输文件
                $params = $multi ? $params : http_build_query($params);
                $opts[CURLOPT_URL] = $url;
                $opts[CURLOPT_POST] = 1;
                $opts[CURLOPT_POSTFIELDS] = $params;
                break;
            default:
                throw new \Exception('不支持的请求方式！');
        }
        
        /* 初始化并执行curl请求 */
        $ch = curl_init();
        curl_setopt_array($ch, $opts);
        $data  = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        if($error) throw new \Exception('请求发生错误：' . $error);
        return  $data;
    }
}