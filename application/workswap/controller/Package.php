<?php
namespace app\workswap\controller;

use data\worksservice\Course;
use data\worksservice\Vmember;
use data\worksservice\VmemberAccount;
use data\service\Member as MemberService;


class Package extends BaseController
{
    
    /** 
     * 套餐列表页 
     * 
     **/
    public function index()
    {
        
        return view($this->style . 'Package/index');
    }
    
    public function controlPackageList(){
        
        $course = new Course;
        
        $condition['status']=array('in','1,2');
        
        $package_list = $course->getPackageList(1, 0, $condition , "sort asc" );
        
        $this->assign('package_list',$package_list['data']);
        
        return view($this->style . 'Package/controlPackageList');
    }
    
    /**
     * 套餐详情页
     *
     **/
    public function packageInfo()
    {
        
        // 获取套餐详情和套餐课列表
        $course = new Course;
        $package_id = request()->get('package_id');
        $package_info = $course->getPackageInfo($package_id);
        
        $package_course_list = $course->getPackageCourseList($package_id);
        
        $this->assign("package_info" , $package_info);
        $this->assign('package_course_list' , $package_course_list);
        
        // 获取讲师列表
        $teacher_id_list = [];
        $lession_num = 0;
        if(!empty($package_course_list)){
            foreach($package_course_list as $key=>$val)
            {
                $lession_num += $val['course_info']['lession_num'];
                foreach($val['course_info']['course_teacher_list'] as $ke=>$vo)
                {
                    if(!in_array($vo['teacher_id'] , $teacher_id_list) && $vo['teacher_id'] != 0)
                    {
                        $teacher_id_list[] = $vo['teacher_id'];
                    }
                }
            }
        }
        
        
        $teacher_id_list = join(',' , $teacher_id_list);
        $condition['id'] = ['in' , $teacher_id_list];
        $teacher_list = $course->getTeacherList(1 , 0 , $condition);
        $this->assign('teacher_list' , $teacher_list['data']);
        $this->assign('lession_num' , $lession_num);
        
        // 获取推荐列表
        $condition_rec['status'] = array('in','1,2');
        $condition_rec['package_id'] = ['neq',$package_id];
        
        $recommend_list = $course->getPackageList(1 , 2 , $condition_rec , 'buy_count+basics_buy desc');
        foreach($recommend_list['data'] as $key=>$val){
            $course_list = $course->getPackageCourseList($val['package_id']);
            $val['course_list'] = $course_list;
        }
        $this->assign('recommend_list' , $recommend_list['data']);
       
        
        // 获取评论和回复列表
        $account = new VmemberAccount;
        $condition_e['relation_id'] = $package_id;
        $condition_e['relation_type'] = 2;
        $condition_e['is_show'] = 1;
        $condition_e['reply_evaluate'] = 0;
         
        $data = $account->getMemberEvaluateList(1,0,$condition_e,'evaluate_id desc');
         
        
        $evaluate_list = $data['data'];
        
        foreach($evaluate_list as $key=>$val){
            
            $condition_r['relation_id'] = $package_id;
            $condition_r['relation_type'] = 2;
            $condition_r['is_show'] = 1;
            $condition_r['reply_evaluate'] = $val['evaluate_id'];
            $val['reply_list'] = $account-> getMemberEvaluateList(1,0,$condition_r,"evaluate_id desc")['data'];
            $val['praise_count'] = $account->setMemberDianzanCount(1, $val['evaluate_id']);
            foreach($val['reply_list'] as $ke=>$va){
                $va['praise_count'] = $account->setMemberDianzanCount(1, $va['evaluate_id']);
            }
        }
         
        $this->assign('evaluate_list',$evaluate_list);
        
        // 获取该套餐是否被收藏
        $account = new VmemberAccount();
        $is_collection = $account->isCourseCollection($package_id,2);
        $this->assign('is_collection',$is_collection);
        
        // 获取会员资料
        $member = new MemberService();
        $member_info = $member->getMemberDetail($this->instance_id);
        if(!$member_info){
            $member_info = [];
        }
        $this->assign('member_info', $member_info);
        
        // 会员是否拥有该套餐
        $vmember_service = new Vmember();
        $is_have_package = $vmember_service->getVmemberHaveIsCourse($package_id , 2);
        $this->assign('is_have_package',$is_have_package);
        
        //dump($is_have_package);
        
        return view($this->style . 'Package/packageInfo');
    }
    
    
}