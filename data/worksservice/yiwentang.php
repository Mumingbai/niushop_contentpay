<?php
/**
 * yiwentang.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksservice;
use data\service\BaseService;
use data\service\Config;
class yiwentang extends Config
{
    
    /**
     * 填写修改客服
     */
    public function addUpdateKefu($cus_qrcode, $cus_phone, $cus_email, $cus_qq, $cus_weixin){
        
        $key = 'YWT_QC';
        $data = array(
            'cus_qrcode' => $cus_qrcode,
            'cus_phone' => $cus_phone,
            'cus_email' => $cus_email,
            'cus_qq' => $cus_qq,
            'cus_weixin' => $cus_weixin
        );
        $value = json_encode($data);
        $config_info = $this->getConfig(0, $key);
        if(empty($config_info)){
            
            $res = $this->addConfig(0, $key, $value, '宜文堂客服二维码', 1);
        }else{
            
            $res = $this->updateConfig(0, $key, $value, '宜文堂客服二维码', 1);
        }
        
        return $res;
    }
    
    /**
     * 获取客服图片
     */
    public function getYwtKefu(){
        
        $key = 'YWT_QC';
        $config_kefu = $this->getConfig(0, $key);
        return $config_kefu['value'];
    }
    
}