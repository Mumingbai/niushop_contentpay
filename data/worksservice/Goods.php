<?php
/**
 * New.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksservice;
use data\service\BaseService;
use data\worksmodel\NvGoodsModel;
use data\worksapi\IGoods;
use think\Log;
use data\model\BaseModel;
use data\worksmodel\NvGoodsClassModel;

class Goods extends BaseService implements IGoods
{
    
    /**
     * 
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     * @return multitype:number unknown
     */
    public function getGoodsList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*'){
        
        $goods_model = new NvGoodsModel();
        //$goods_list = $goods_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        //$goods_list = $goods_model->getViewQuery($page_index, $page_size, $condition, $order);
        $goods_list = $goods_model->getViewList($page_index, $page_size, $condition, $order);
        Log::write("1----1");
        return $goods_list;
    }
    /**
     * 添加修改商品
     * @param unknown $goods_data
     */
    public function addUpdateGoods($goods_data){
        $goods_model = new NvGoodsModel();
        $goods_model->startTrans();
        try {
            $goods_id = $goods_data['goods_id'];
            if(empty($goods_id)){
        
                $goods_data['create_time'] = time();
                $goods_id = $goods_model->save($goods_data);
            }else{
                $goods_data['modify_time'] = time();
                $res = $goods_model->save($goods_data, ['goods_id' => $goods_id]);
            }
            $goods_model->commit();
            return $goods_id;
        
        } catch (\Exception $e) {
            $goods_model->rollback();
            return $e->getMessage();
        }
    }
    
    /**
     * 删除商品
     * @param unknown $goods_ids
     * @param string $condition
     */
    public function delGoods($goods_ids, $condition = ''){
        if(!empty($goods_ids)) $condition['goods_id'] = array('in', $goods_ids);
        
        $goods_model = new NvGoodsModel();
        $res = $goods_model->destroy($condition);
        return $res;
    }
    
    
    
    public function getGoodsInfo($goods_id, $condition = '', $field = '*'){
        $goods_model = new NvGoodsModel();
        
        if(!empty($goods_id)) $condition['goods_id'] = $goods_id;
        if(empty($condition)) return false;
        
        $goods_info = $goods_model->getInfo($condition, $field);
        return $goods_info;
    }
    
    /**
     * 添加修改商品分类
     * @param unknown $goods_data
     */
    function addUpdateGoodsClass($goods_class_data){
        
        $goods_clsss_model = new NvGoodsClassModel();
        $class_id = $goods_class_data['class_id'];
        
        if(!empty($class_id)){
            $res = $goods_clsss_model->save($goods_class_data, ['class_id'=>$class_id]);
        }else{
            $res = $goods_clsss_model->save($goods_class_data);
        }
        
        return $res;
        
    }
    
    /**
     * 删除商品分类
     * @param unknown $goods_ids
     * @param string $condition
    */
    function delGoodsClass($goods_class_ids, $condition = ''){
        
        if(!empty($goods_class_ids)) $condition['class_id'] = array('in', $goods_class_ids);
        if(empty($condition)) return false;
        
        $goods_clsss_model = new NvGoodsClassModel();
        $retval=$goods_clsss_model->destroy($condition);
        
        return $retval;
    }
    
    /**
     * 获取商品分类列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field  */
    function getGoodsClassList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*'){
        
        $goods_clsss_model = new NvGoodsClassModel();
        $goods_class_list = $goods_clsss_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        
        return $goods_class_list;
    }
    
    /**
     * 获取商品分类详情
     * @param unknown $goods_class_id
     * @param unknown $condition
     * @param unknown $field  */
    function getGoodsClassInfo($goods_class_id, $condition='', $field='*'){
    
        if(!empty($goods_class_id)) $condition['class_id'] = $goods_class_id;
        if(empty($condition)) return false;
        
        $goods_class_model = new NvGoodsClassModel();
        $goods_class_info = $goods_class_model->getInfo($condition, $field='*');
        
        return $goods_class_info;
    
    }
    
  
   
}
