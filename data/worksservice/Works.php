<?php
/**
 * Works.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksservice;
use data\service\BaseService;
use data\worksmodel\NvDbClassModel;
use data\worksmodel\NvDbWorksModel;
use data\service\Upload\QiNiu;
use data\service\Config as WebConfig;
use think\Log;
class Works extends BaseService implements \data\worksapi\IWorks
{
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IWorks::addUpdateDbClass()
     */
    public function addUpdateDbClass($db_class_data){
        
        $class_id = $db_class_data['class_id'];
        
        $db_class_model = new NvDbClassModel();
        

        $res = '';
        if(empty($class_id)){
            
            $db_class_data['create_time'] = time();
            $res = $db_class_model->save($db_class_data);
            $class_id = $res;
        }else{
            
            $res = $db_class_model->save($db_class_data, ['class_id' => $class_id]);
        }
        
        if($db_class_data['is_default'] == 1){
            $this->setDbClassDefault($class_id);
        }
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IWorks::setDbClassDefault()
     */
    public function setDbClassDefault($class_id, $condition = ''){
     
        if(!empty($class_id)) $condition['class_id'] = $class_id;
        
        $db_class_model = new NvDbClassModel();
        $db_class_model->save(['is_default'=>0],'1=1');
        $res = $db_class_model->save(['is_default'=>1], $condition);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IWorks::delDbClass()
     */
    public function delDbClass($class_id, $condition = ''){
        
        if(!empty($class_id)) $condition['class_id'] = $class_id;
        
        $db_class_model = new NvDbClassModel();
        $db_class_model->startTrans();
        
        try {
        
            $this->delDbWorks(0, ['class_id' => $condition['class_id']]);
            
            $res = $db_class_model->destroy($condition);
            
            $db_class_model->commit();
            return $res;
            
        } catch (\Exception $e) {
            
            $db_class_model->rollback();
            return $e->getMessage();
        }
        
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IWorks::getDbClassInfo()
     */
    public function getDbClassInfo($class_id, $condition = '', $filed = '*'){
        
        if(!empty($class_id)) $condition['class_id'] = $class_id;
        
        if(empty($condition)) return false;
        
        $db_class_model = new NvDbClassModel();
        $db_class_info = $db_class_model->getInfo($condition, $filed);
        return $db_class_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IWorks::getDbClassList()
     */
    public function getDbClassList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*'){
        
        $db_class_model = new NvDbClassModel();
        $db_class_list = $db_class_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        
        foreach($db_class_list['data'] as $item){
            
            $item['works_count'] = $this->getDbClassWorksCount($item['class_id']);
        }
        return $db_class_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IWorks::addUpdateDbWorks()
     */
    public function addUpdateDbWorks($db_works_data){
        
        $db_works_model = new NvDbWorksModel();
        
        $works_id = $db_works_data['works_id'];
        
        $config = new WebConfig();
        $upload_type = $config->getUploadType(0);
        
        if($upload_type == 2){
            
            $db_works_data['works_type'] = 3;
        }else{
            $db_works_data['works_type'] = 1;
        }
        //七牛获取编号
        $qiniu_service = new QiNiu();
        $db_works_data['yun_code'] = $qiniu_service->getQiniuUrlByKey($db_works_data['works_url']);
         
        $file_info = $qiniu_service->getQiniuUplaodFileInfo($db_works_data['yun_code']);
        $db_works_data['time_length'] = $file_info->streams[0]->duration;
        $res = '';
        if(empty($works_id)){
            
            $db_works_data['works_code']  = $this->createWorksCode();
            
            $db_works_data['create_time'] = time();
            $res = $db_works_model->save($db_works_data);
        }else{
            
            $res = $db_works_model->save($db_works_data, ['works_id'=> $works_id]);
        }
        return $res;
    }
    
    /**
     * 生成视频码
     */
    public function createWorksCode(){
        
        $works_code = md5('nkxi'.md5(time()));
        $works_info = $this->getDbWorksInfo(0, ['works_code' => $works_code]);
        while (true){
            if(empty($works_info)){
                break;
            }else{
                $works_code = $this->createWorksCode();
            }
        }
        return $works_code;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IWorks::delDbWorks()
     */
    public function delDbWorks($works_id, $condition = ''){
        
        if(!empty($works_id)) $condition['works_id'] = $works_id;
        
        $db_works_model = new NvDbWorksModel();
        $res = $db_works_model->destroy($condition);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IWorks::getDbWorksInfo()
     */
    public function getDbWorksInfo($works_id, $condition = '', $filed = '*'){
        
        if(!empty($works_id)) $condition['works_id'] = $works_id;
        if(empty($condition)) return false;
        
        $db_works_model = new NvDbWorksModel();
        $db_works_info = $db_works_model->getInfo($condition, $filed);
        return $db_works_info;
        
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IWorks::getDbWorksList()
     */
    public function getDbWorksList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*'){
        
        $db_works_model = new NvDbWorksModel();
        $db_works_list = $db_works_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        
        foreach($db_works_list['data'] as $item){
            
            $item['create_time'] = getTimeStampTurnTime($item['create_time']);
        }
        return $db_works_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IWorks::getDbClassWorksCount()
     */
    public function getDbClassWorksCount($class_id, $condition = ''){
        
        if(!empty($class_id)) $condition['class_id'] = $class_id;
        
        $db_works_model = new NvDbWorksModel();
        $works_count = $db_works_model->getCount($condition);
        return $works_count;
    }
}