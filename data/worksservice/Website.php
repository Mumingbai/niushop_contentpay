<?php
/**
 * New.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksservice;
use data\service\BaseService;
use data\worksapi\IWebsite;
use data\worksmodel\NvWebsiteNavigationModel;
use data\worksmodel\NvSubjectHomeBlockModel;
use data\worksmodel\NvWebsiteAdvPositionModel;
use data\worksmodel\NvWebsiteAdvModel;
use data\worksmodel\NvWebsiteNavigationPositionModel;
use data\worksmodel\NvHomePlateModel;
use data\worksmodel\NvHomePlateWorksModel;
use think\Log;

class Website extends BaseService implements IWebsite
{
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::addUpdateWebsiteNavigationPosition()
     */
    public function addUpdateWebsiteNavigationPosition($data){
     
        $navigation_position_model = new NvWebsiteNavigationPositionModel();
        
        $data['modify_time'] = time();
        
        $position_id = $data['position_id'];
        if(empty($position_id)){
            
            $data['create_time'] = time();
            $position_id = $navigation_position_model->save($data);
        }else{
            
            $navigation_position_model->save($data, ['position_id' => $position_id]);
        }
        return $position_id;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::delWebsiteNavigationPosition()
     */
    public function delWebsiteNavigationPosition($position_ids, $condition = ''){
        
        //条件性初步判断
        if(!empty($position_ids)) $condition['position_id'] = array('in',$position_ids);;
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $condition['is_system'] = 0;    //非系统的才可进行删除
        
        $navigation_position_model = new NvWebsiteNavigationPositionModel();
        $navigation_position_model->startTrans();
        
        try {
            
            $navigation_position_model->destroy($condition);
            
            //如果导航位下有导航信息那就一块删除
            $navigation_position_list = $navigation_position_model->getQuery($condition, 'position_id', 'sort asc');
            foreach ($navigation_position_list as $item){
                $this->delWebsiteNavigation(0, ['position_id' => $item['position_id']]);
            }
            
            $navigation_position_model->commit();
            return 1;
        } catch (\Exception $e) {
            
            $navigation_position_model->rollback();
            Log::write('导航位删除失败，错误：'.$e->getMessage());
            return $e->getMessage();
        }
        
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getWebsiteNavigationPositionInfo()
     */
    public function getWebsiteNavigationPositionInfo($position_id, $condition = '', $field = '*'){
        
        //条件性初步判断
        if(!empty($position_id)) $condition['position_id'] = $position_id;
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $navigation_position_model = new NvWebsiteNavigationPositionModel();
        $navigation_position_info = $navigation_position_model->getInfo($condition, $field);
        return $navigation_position_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getWebsiteNavigationPositionList()
     */
    public function getWebsiteNavigationPositionList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
        
        $navigation_position_model = new NvWebsiteNavigationPositionModel();
        $navigation_position_list = $navigation_position_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        return $navigation_position_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getWebsiteNavigationPositionDetail()
     */
    public function getWebsiteNavigationPositionDetail($position_id){
        
        $navigation_position_info = $this->getWebsiteNavigationPositionInfo($position_id);
        
        $website_navigation_model = new NvWebsiteNavigationModel();
        $navigation_position_info['navigation_list'] = $website_navigation_model->getQuery(['position_id'=>$position_id, 'is_block'=>1], '*', 'sort asc');
        return $navigation_position_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::addUpdateWebsiteNavigation()
     */
    public function addUpdateWebsiteNavigation($data){
       
        $website_navigation_model = new NvWebsiteNavigationModel(); 
        
        $data['modify_time'] = time();
        
        $nav_id = $data['nav_id'];
        if(empty($nav_id)){
            
            $data['create_time'] = time();
            $nav_id = $website_navigation_model->save($data);
            
        }else{
            
            $website_navigation_model->save($data, ['nav_id' => $nav_id]);
        }
        return $nav_id;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::delWebsiteNavigation()
     */
    public function delWebsiteNavigation($nav_id, $condition = ''){
       
        if(!empty($nav_id)) $condition['nav_id'] = $nav_id;
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $website_navigation_model = new NvWebsiteNavigationModel();
        $res = $website_navigation_model->destroy($condition);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getWebsiteNavigationInfo()
     */
    public function getWebsiteNavigationInfo($nav_id, $condition = '', $field = '*'){
        
        if(!empty($nav_id)) $condition['nav_id'] = $nav_id;
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $website_navigation_model = new NvWebsiteNavigationModel();
        $website_navigation_info = $website_navigation_model->getInfo($condition, $field);
        return $website_navigation_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getWebsiteNavigationList()
     */
    public function getWebsiteNavigationList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
        
        $website_navigation_model = new NvWebsiteNavigationModel();
        $website_navigation_list = $website_navigation_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        return $website_navigation_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::addUpdateSubjectHomeBlock()
     */
    public function addUpdateSubjectHomeBlock($data){
        
        $subject_home_block_model = new NvSubjectHomeBlockModel();
        
        $subject_id = $data['subject_id'];
        $subject_home_block_info = $this->getSubjectHomeBlockInfo($subject_id);
        
        if(empty($subject_home_block_info)){
           
            $res = $subject_home_block_model->save($data);
        }else{
            
            $res = $subject_home_block_model->save($data, ['subject_id' => $subject_id]);
        }
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getSubjectHomeBlockInfo()
     */
    public function getSubjectHomeBlockInfo($subject_id, $condition = '', $field = '*'){
       
        if(!empty($subject_id)) $condition['subject_id'] = $subject_id;
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $subject_home_block_model = new NvSubjectHomeBlockModel();
        $subject_home_block_info = $subject_home_block_model->getInfo($condition, $field); 
        return $subject_home_block_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::delSubjectHomeBlock()
     */
    public function delSubjectHomeBlock($subject_id, $condition = ''){
        
        if(!empty($subject_id)) $condition['subject_id'] = $subject_id;
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $subject_home_block_model = new NvSubjectHomeBlockModel();
        $res = $subject_home_block_model->destroy($condition);
        
        //如果有该专业有下级也将删除
        $children_subject_list = $subject_home_block_model->getQuery(['parent_subject_id' => $subject_id], 'subject_id', 'subject_id asc');
        
        if(!empty($children_subject_list)){
            
            $id_list = [];
            foreach ($children_subject_list as $item){
                
                $id_list[] = $item['subject_id'];
            }
            
            $id_list = join(',', $id_list);
            $condition['subject_id'] = ['in', $id_list];
            
            $child_del_res = $subject_home_block_model->destroy($condition);
            
        }
        
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getSubjectHomeBlockList()
     */
    public function getSubjectHomeBlockList($condition = ['parent_subject_id'=>0], $order = 'sort asc'){
        
        $subject_home_block_model = new NvSubjectHomeBlockModel();
        $home_block_list = $subject_home_block_model->getQuery($condition, '*', $order);
        
        foreach($home_block_list as $item){
            
            $item['two_subject_list'] = $this->getSubjectHomeBlockList(['parent_subject_id' => $item['subject_id']]);
            
        }
        return $home_block_list;
        
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getSubjectHomeBlock()
     */
    public function getSubjectHomeBlock($parent_subject_id = 0, $order = 'sort asc'){
        
        $subject_home_block_model = new NvSubjectHomeBlockModel();
        $home_block_list = $subject_home_block_model->getQuery(['parent_subject_id'=>$parent_subject_id,'is_show'=>1], '*', $order);
        
        foreach($home_block_list as $item){
        
            $item['two_subject_list'] = $this->getSubjectHomeBlock($item['subject_id'], 'sort asc');
            
            $course_service = new Course();
            if($parent_subject_id == 0){
                $item['course_list'] = $course_service->getCourseList(1,6,['subject_id_1'=>$item['subject_id']])['data'];
            }else{
                $item['course_list'] = $course_service->getCourseList(1,6,['subject_id_2'=>$item['subject_id']])['data'];
            }
            
            $item['ad_picture'] = json_decode($item['ad_picture'], true);
        }
        return $home_block_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::updateSubjectHomeBlockAd()
     */
    public function updateSubjectHomeBlockAd($id, $data){
        
        $subject_home_block_model = new NvSubjectHomeBlockModel();
        $res = $subject_home_block_model->save(['ad_picture' => $data], ['subject_id' => $id]);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::addUpdateWebsiteAdvPosition()
     */
    public function addUpdateWebsiteAdvPosition($data){
       
        //查看当前广告位名称是否存在
        $adv_position_info = $this->getWebsiteAdvPositionInfo(0, ['ap_name' => $data['ap_name']], 'ap_id');
        
        $website_adv_position_model = new NvWebsiteAdvPositionModel();
        
        $ap_id = $data['ap_id'];
        if(empty($ap_id)){
            
            $ap_id = $website_adv_position_model->save($data);
        }else{
            
            $website_adv_position_model->save($data, ['ap_id' => $ap_id]);
        }
        
        return $ap_id;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::delWebsiteAdvPosition()
     */
    public function delWebsiteAdvPosition($ap_ids, $condition = ''){
        
        if(!empty($ap_ids)) $condition['ap_id'] = array('in',$ap_ids);
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $website_adv_position_model = new NvWebsiteAdvPositionModel();
        $website_adv_position_model->startTrans();
        
        try {
            
            $condition['is_system'] = 0;    //删除非系统广告位
            $website_adv_position_model->destroy($condition);
            
            $website_adv_positio_list = $website_adv_position_model->getQuery($condition, 'ap_id', 'sort asc');
            foreach ($website_adv_positio_list as $item){
                
                //删除该广告位下的广告
                $this->delWebsiteAdv(0, ['ap_id'=> $item['ap_id']]);
            }
            
            $website_adv_position_model->commit();
            return 1;
            
        } catch (\Exception $e) {
            
            $website_adv_position_model->rollback();
            Log::write('删除广告位失败，错误：'.$e->getMessage());
            return $e->getMessage();
        }
       
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getWebsiteAdvPositionInfo()
     */
    public function getWebsiteAdvPositionInfo($ap_id, $condition = '', $field = '*'){
        
        if(!empty($ap_id)) $condition['ap_id'] = $ap_id;
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $website_adv_position_model = new NvWebsiteAdvPositionModel();
        $adv_position_info = $website_adv_position_model->getInfo($condition, $field);
        return $adv_position_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getWebsiteAdvPositionList()
     */
    public function getWebsiteAdvPositionList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
        
        $website_adv_position_model = new NvWebsiteAdvPositionModel();
        $adv_position_list = $website_adv_position_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        return $adv_position_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::addUpdateWebsiteAdv()
     */
    public function addUpdateWebsiteAdv($data){
       
        $website_adv_model = new NvWebsiteAdvModel();
        
        $adv_id = $data['adv_id'];
        if(empty($adv_id)){
            
            $adv_id = $website_adv_model->save($data);
        }else{
            
            $website_adv_model->save($data, ['adv_id'=> $adv_id]); 
        }
        return $adv_id;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::delWebsiteAdv()
     */
    public function delWebsiteAdv($adv_id, $condition = ''){
        
        if(!empty($adv_id)) $condition['adv_id'] = $adv_id;
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $website_adv_model = new NvWebsiteAdvModel();
        $res = $website_adv_model->destroy($condition);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getWebsiteAdvInfo()
     */
    public function getWebsiteAdvInfo($adv_id, $condition = '', $field = '*'){
        
        if(!empty($adv_id)) $condition['adv_id'] = $adv_id;
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $website_adv_model = new NvWebsiteAdvModel();
        $website_adv_info = $website_adv_model->getInfo($condition, $field);
        return $website_adv_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getWebsiteAdvList()
     */
    public function getWebsiteAdvList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
        
        $website_adv_model = new NvWebsiteAdvModel();
        $website_adv_list = $website_adv_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        return $website_adv_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::setWebsiteAdvPositionAdvSum()
     */
    public function setWebsiteAdvPositionAdvSum($ap_id, $condition = ''){
       
        $website_adv_count = $this->getWebsiteAdvCount($ap_id, $condition);
        $res = $this->addUpdateWebsiteAdvPosition(['ap_id'=>$ap_id,'adv_num'=> $website_adv_count]);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getWebsiteAdvCount()
     */
    public function getWebsiteAdvCount($ap_id, $condition = ''){
        
        if(!empty($ap_id)) $condition['ap_id'] = $ap_id;
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $website_adv_model = new NvWebsiteAdvModel();
        $website_adv_count = $website_adv_model->getCount($condition);
        return $website_adv_count;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getWebsiteAdvPositionDetail()
     */
    public function getWebsiteAdvPositionDetail($ap_id){
       
        $adv_position_info = $this->getWebsiteAdvPositionInfo($ap_id);
        
        $website_adv_model = new NvWebsiteAdvModel();
        $adv_position_info['adv_list'] = $website_adv_model->getQuery(['ap_id'=> $ap_id, 'is_block'=> 1], '*', 'slide_sort asc');
        return $adv_position_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::addUpdateHomePlate()
     */
    public function addUpdateHomePlate($data){
        
        $home_plate_model = new NvHomePlateModel();
        
        $plate_id = $data['plate_id'];
        if(empty($plate_id)){
            
            $plate_id = $home_plate_model->save($data);
        }else {
            $home_plate_model->save($data, ['plate_id'=>$plate_id]);
        }
        return $plate_id;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::delHomePlate()
     */
    public function delHomePlate($plate_id, $condition = ''){
        
        if(!empty($plate_id)) $condition['plate_id'] = array('in',$plate_id);
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $home_plate_model = new NvHomePlateModel();
        $home_plate_model->startTrans();
        
        try {
         
            $home_plate_model->destroy($condition);
            
            //删除该板块下的作品
            $this->delHomePlateWorks(0, $condition);
            
            $home_plate_model->commit();
            return 1;
        } catch (\Exception $e) {
            
            $home_plate_model->rollback();
            Log::write('删除改板块出错，错误：'.$e->getMessage());
            return $e->getMessage();
        }
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getHomePlate()
     */
    public function getHomePlate($plate_id, $condition = '', $field = '*'){
        
        if(!empty($plate_id)) $condition['plate_id'] = $plate_id;
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $home_plate_model = new NvHomePlateModel();
        $home_plate_info = $home_plate_model->getInfo($condition, $field);
        
        if(!empty($home_plate_info)){
            
            $home_plate_info['plate_works_list'] = $this->getHomePlateWorksList($home_plate_info['plate_id']);
        }
        
        return $home_plate_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getHomePlateList()
     */
    public function getHomePlateList($codition = '1=1'){
        
        $home_plate_model = new NvHomePlateModel();
        $home_plate_list = $home_plate_model->getQuery($codition, '*', 'sort asc');
        
        foreach($home_plate_list as $item){
            
            $item['plate_works_list'] = $this->getHomePlateWorksList($item['plate_id']);
        }
        
        return $home_plate_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::updateHomePlateWorksSort()
     */
    public function updateHomePlateWorksSort($pr_id, $sort){
        
        $home_plate_works_model = new NvHomePlateWorksModel();
        $res = $home_plate_works_model->save(['sort' => $sort], ['pr_id' => $pr_id]);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::addBatchHomePlateWorks()
     */
    public function addBatchHomePlateWorks($plate_id, $relation_ids){
       
        //$this->delHomePlateWorks(0,['plate_id'=>$plate_id]);
        $relation_id_arr = explode(',', $relation_ids);
        foreach($relation_id_arr as $item){
            
            $data = array(
                'plate_id'  => $plate_id,
                'relation_id'   => $item
            );
            $home_plate_works_model = new NvHomePlateWorksModel();
            $home_plate_works_model->save($data);
        }
        return 1;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::getHomePlateWorksList()
     */
    public function getHomePlateWorksList($plate_id, $relation_type = 1){
        
        $home_plate_works_model = new NvHomePlateWorksModel();
        $home_plate_works_list = $home_plate_works_model->getQuery(['plate_id' => $plate_id], '*', 'sort asc');
        
        foreach($home_plate_works_list as $item){
            
            if($relation_type == 1){
                
                $course_service = new Course();
                $item['info'] = $course_service->getCourseInfo($item['relation_id']);
            }else if($relation_type == 2){
                
            }
        }
        return $home_plate_works_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IWebsite::delHomePlateWorks()
     */
    public function delHomePlateWorks($pr_id, $condition = ''){
        
        if(!empty($pr_id)) $condition['pr_id'] = array('in',$pr_id);
        $home_plate_works_model = new NvHomePlateWorksModel();
        $res = $home_plate_works_model->destroy($condition);
        return $res;
    }

}