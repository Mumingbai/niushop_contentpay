<?php
/**
 * Operate.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksservice;
use data\service\BaseService;
use data\worksapi\IOperate;
use data\worksmodel\NvCouponModel;
use data\worksmodel\NvMemberCouponModel;
/**
 * 运营
 * @author lzw
 *
 */
class Operate extends BaseService implements IOperate
{
 
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOperate::addUpdateCoupon()
     */
    public function addUpdateCoupon($data){
       
        $coupon_model = new NvCouponModel();
        
        if($data['receive_end_type'] == 2){
            
            $data['receive_end_time'] = getTimeTurnTimeStamp($data['receive_end_time']);
        }
        $data['coupon_end_time'] = getTimeTurnTimeStamp($data['coupon_end_time']);
        $data['coupon_start_time'] = getTimeTurnTimeStamp($data['coupon_start_time']);
        
        $data['surplus_num'] = $data['total_num']; 
        $vcoupon_id = $data['vcoupon_id'];
        if(empty($vcoupon_id)){
            
            $data['create_time'] = time();
            $vcoupon_id = $coupon_model->save($data);
        }else{
            
            $coupon_model->save($data, ['vcoupon_id'=>$vcoupon_id]);
        }
        
        return $vcoupon_id;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOperate::getCouponInfo()
     */
    public function getCouponInfo($vcoupon_id, $condition = '', $field = '*'){
        
        if(!empty($vcoupon_id)) $condition['vcoupon_id'] = $vcoupon_id;
        
        $coupon_model = new NvCouponModel();
        $coupon_info = $coupon_model->getInfo($condition,'*');
        
        if(!empty($coupon_info)){
            
            if($coupon_info['receive_end_type'] == 2){
                
//                 $coupon_info['receive_end_time'] = getTimeStampTurnTime($coupon_info['receive_end_time']);
            }
             
//             $coupon_info['coupon_end_time'] = getTimeStampTurnTime($coupon_info['coupon_end_time']);
//             $coupon_info['coupon_start_time'] = getTimeStampTurnTime($coupon_info['coupon_start_time']);
        }
        return $coupon_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOperate::getCouponList()
     */
    public function getCouponList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
        
        $condition['status'] = array('neq', '-2'); //如果是已经删除的不显示
        $coupon_model = new NvCouponModel();
        $coupon_list = $coupon_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        
        //完善列表页面显示信息
        $coupon_list['data'] = $this->getCouponStatusList($coupon_list['data']);
        
        return $coupon_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOperate::delCoupon()
     */
    public function delCoupon($vcoupon_ids){
        
        $coupon_model = new NvCouponModel();

        $condition = array(
            'vcoupon_id' => array('in',$vcoupon_ids),
            'status' => 0
        ); 
        $coupon_model->destroy($condition);
        
        $condition['status'] = 2;
        $data = array('status' => -2);
        $coupon_model = new NvCouponModel();
        $coupon_model->save($data, $condition);
        return 1;
    }
    
    /**
     * 优惠劵状态显示完善
     * @param unknown $list
     */
    private function getCouponStatusList($list){
        
        foreach($list as $item){
        
            //优惠劵状态
            switch ($item['status']) {
                case 1:
                    $item['status_name'] = '开始';
                    break;
                case 2:
                    $item['status_name'] = '结束';
                    break;
                case -1:
                    $item['status_name'] = '停止';
                    break;
                default:
                    $item['status_name'] = '未开始';
                    break;
            }
        
            //优惠内容显示
            $item['discount_name'] = $this->getCouponDiscountName($item['condition_money'], $item['discount_type'], $item['type_value']);
          
            if($item['receive_end_type'] == 1){
                
                $item['receive_end_time'] = '领取后'. $item['receive_end_time']. '天';
            }elseif($item['receive_end_type'] == 2){
                
                $item['receive_end_time'] = getTimeStampTurnTime($item['receive_end_time']);
            }
            
            $item['coupon_end_time'] = getTimeStampTurnTime($item['coupon_end_time']);
            $item['coupon_start_time'] = getTimeStampTurnTime($item['coupon_start_time']);
        }
        
        return $list;
    }
    
    /**
     * 根据类型和值拼接优惠劵的优惠信息
     * @param unknown $discount_type
     * @param unknown $type_value
     */
    public function getCouponDiscountName($condition_money, $discount_type, $type_value){
        
        $discount_name = '';
        if($condition_money == 0.00){
            $discount_name = '满' . $condition_money . '元';
        }
        if($discount_type == 1){
        
            $discount_name .= '减' . $type_value . '元';
        
        }elseif($discount_type == 2){
        
            $discount_name .= '打' . $type_value . '折';
        }
        
        return $discount_name;
        
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOperate::getProsceniumCouponList()
     */
    public function getProsceniumCouponList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
        
        $condition = array('status'=> 1);
        $coupon_model = new NvCouponModel();
        $coupon_list = $coupon_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        
        //完善列表页面显示信息
        $coupon_list['data'] = $this->getCouponStatusList($coupon_list['data']);
        
        return $coupon_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOperate::updateCouponStatus()
     */
    public function updateCouponStatus($vcoupon_id, $status = 0){
        
        $coupon_model = new NvCouponModel();
        $res = $coupon_model->save(['status' => $status], ['vcoupon_id' => $vcoupon_id]);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOperate::setCouponSurplusNum()
     */
    public function setCouponSurplusNum($vcoupon_id){
        
        //计算已被领取的优惠劵
        $vmember_account = new NvMemberCouponModel();
        $use_coupon_count = $vmember_account->getCount(['coupon_id'=>$vcoupon_id]);
        
        //当前优惠活动的信息
        $coupon_info = $this->getCouponInfo($vcoupon_id);
        
        //剩余优惠劵数量
        $surplus_num = $coupon_info['total_num'] - $use_coupon_count;
        $data['surplus_num'] = $surplus_num;
        
        //如果剩余的优惠劵数量为0 代表活动也结束
        if($surplus_num == 0) $data['status'] = 2;
        
        $coupon_model = new NvCouponModel();
        $res = $coupon_model->save($data, ['vcoupon_id' => $vcoupon_id]);
        return $res;
    }
}
