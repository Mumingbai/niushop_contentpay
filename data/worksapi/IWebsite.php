<?php
/**
 * INew.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksapi;
interface IWebsite
{
    
    /**
     * 添加修改网站
     * @param unknown $data
     */
    function addUpdateWebsiteNavigationPosition($data);
    
    /**
     * 删除导航位
     * @param unknown $position_id
     * @param string $condition
     */
    function delWebsiteNavigationPosition($position_id, $condition = '');
    
    /**
     * 获取导航位详情信息 
     * @param unknown $position_id
     * @param string $condition
     * @param string $field
     */
    function getWebsiteNavigationPositionInfo($position_id, $condition = '', $field = '*');
    
    /**
     *  获取导航位列表信息
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getWebsiteNavigationPositionList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*');
    
    /**
     * 获取导航位包括导航位下的导航信息
     * @param unknown $position_id
     */
    function getWebsiteNavigationPositionDetail($position_id);
    
    /**
     * 添加修改网站导航位导航
     * @param unknown $data
     */
    function addUpdateWebsiteNavigation($data);
    
    /**
     * 删除网站导航位导航
     * @param unknown $nav_id
     * @param string $condition
     */
    function delWebsiteNavigation($nav_id, $condition = '');
    
    /**
     * 获取网站导航位导航
     * @param unknown $nav_id
     * @param string $condition
     * @param string $field
     */
    function getWebsiteNavigationInfo($nav_id, $condition = '', $field = '*');
    
    /**
     * 获取网站导航位导航
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getWebsiteNavigationList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*');
    
    /**
     * 添加修改网站首页专业显示
     * @param unknown $data
     */
    function addUpdateSubjectHomeBlock($data);
    
    /**
     * 删除网站首页专业显示
     * @param unknown $subject_id
     * @param string $condition
     */
    function delSubjectHomeBlock($subject_id, $condition = '');
    
    /**
     * 获取网站首页专业显示
     * @param unknown $subject_id
     * @param string $condition
     * @param string $field
     */
    function getSubjectHomeBlockInfo($subject_id, $condition = '', $field = '*');
    
    /**
     * 获取网站首页专业显示列表
     * @param string $condition
     */
    function getSubjectHomeBlockList($condition = ['parent_subject_id'=>0], $order = 'sort asc');
    
    /**
     * 首页直接使用显示 
     */
    function getSubjectHomeBlock($parent_subject_id = 0, $order = 'sort asc');
    
    /**
     * 修改专业楼层板块的广告信息
     * @param unknown $data
     */
    function updateSubjectHomeBlockAd($id, $data);
    
    /**
     * 添加修改广告位
     * @param unknown $data
     */
    function addUpdateWebsiteAdvPosition($data);
    
    /**
     * 删除广告位
     * @param unknown $ap_id
     * @param string $condition
     */
    function delWebsiteAdvPosition($ap_id, $condition = '');
    
    /**
     * 获取广告位详情
     * @param unknown $ap_id
     * @param string $condition
     * @param string $field
     */
    function getWebsiteAdvPositionInfo($ap_id, $condition = '', $field = '*');
    
    /**
     * 获取广告位列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getWebsiteAdvPositionList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*');
    
    /**
     * 广告位广告数量计算
     * @param unknown $ap_id
     * @param string $condition
     */
    function setWebsiteAdvPositionAdvSum($ap_id, $condition = '');
    
    /**
     * 添加修改广告
     * @param unknown $data
     */
    function addUpdateWebsiteAdv($data);
    
    /**
     * 删除广告
     * @param unknown $adv_id
     * @param string $condition
     */
    function delWebsiteAdv($adv_id, $condition = '');
    
    /**
     * 获取广告详情
     * @param unknown $adv_id
     * @param string $condition
     */
    function getWebsiteAdvInfo($adv_id, $condition = '');
    
    /**
     * 获取广告列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getWebsiteAdvList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*');
    
    /**
     * 获取广告数量
     * @param unknown $ap_id
     * @param string $condition
     */
    function getWebsiteAdvCount($ap_id, $condition = '');
    
    /**
     * 获取广告位详情信息（包括广告位下的广告）
     * @param unknown $ap_id
     */
    function getWebsiteAdvPositionDetail($ap_id);
    
    /**
     * 添加修改首页板块
     * @param unknown $data
     */
    function addUpdateHomePlate($data);
    
    /**
     * 删除首页板块
     * @param unknown $plate_id
     * @param string $condition
     */
    function delHomePlate($plate_id, $condition = '');
    
    /**
     * 获取首页板块详情信息
     * @param unknown $plate_id
     * @param string $condition
     * @param string $field
     */
    function getHomePlate($plate_id, $condition = '', $field = '*');
    
    /**
     * 获取板块列表
     * @param string $condition
     */
    function getHomePlateList($condition = '1=1');
    
    /**
     * 首页板块涉及的作品排序修改
     * @param unknown $data
     */
    function updateHomePlateWorksSort($pr_id, $sort);
    
    /**
     * 批量添加板块涉及的作品
     * @param unknown $plate_id
     * @param unknown $relation_ids
     */
    function addBatchHomePlateWorks($plate_id, $relation_ids);
    
    /**
     * 删除板块设置的作品
     * @param string $condition
     */
    function delHomePlateWorks($pr_id, $condition = '');
    
    /**
     * 获取该板块下的作品
     * @param unknown $plate_id
     */
    function getHomePlateWorksList($plate_id, $relation_type = 1);
}