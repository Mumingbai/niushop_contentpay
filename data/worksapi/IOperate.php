<?php
/**
 * IOperate.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksapi;
/**
 * 运营
 * @author lzw
 *
 */
interface IOperate
{
    
    /**
     * 添加修改优惠劵信息
     * @param unknown $data
     */
    function addUpdateCoupon($data);
    
    /**
     * 获取优惠劵列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getCouponList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*');
    
    /**
     * 获取优惠劵信息
     * @param unknown $vcoupon_id
     */
    function getCouponInfo($vcoupon_id, $condition = '', $field = '*');
    
    /**
     * 删除优惠劵信息
     * @param unknown $vcoupon_id
     */
    function delCoupon($vcoupon_ids);
    
    /**
     * 修改优惠劵状态
     * @param unknown $vcoupon_id
     * @param number $status
     */
    function updateCouponStatus($vcoupon_id, $status = 0);
    
    /**
     * 前台显示的优惠劵列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getProsceniumCouponList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*');
    
    /**
     * 计算修改优惠劵的剩余数量
     * @param unknown $vcoupon_id
     */
    function setCouponSurplusNum($vcoupon_id);
}