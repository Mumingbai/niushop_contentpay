<?php
/**
 * ICourse.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksapi;
interface IGoods
{
    /**
     * 获取商品列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getGoodsList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*');
    /**
     * 添加修改商品
     * @param unknown $goods_data
     */
    function addUpdateGoods($goods_data);
    
    /**
     * 删除商品
     * @param unknown $goods_ids
     * @param string $condition
     */
    function delGoods($goods_ids, $condition = '');
    
    /**
     * 获取商品详情
     * @param unknown $goods_id
     * @param string $condition
     * @param string $field
     */
    function getGoodsInfo($goods_id, $condition = '', $field = '*');
    
    /**
     * 添加修改商品分类
     * @param unknown $goods_data
     */
    function addUpdateGoodsClass($goods_class_data);
    
    /**
     * 删除商品分类
     * @param unknown $goods_ids
     * @param string $condition
    */
    function delGoodsClass($goods_class_ids, $condition = '');
    
    /**
     * 获取商品分类列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field  */
    function getGoodsClassList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*');
    
    /**
     * 获取商品分类详情
     * @param unknown $goods_class_id
     * @param unknown $condition
     * @param unknown $field  */
    function getGoodsClassInfo($goods_class_id, $condition='', $field='*');
    
   
    
}