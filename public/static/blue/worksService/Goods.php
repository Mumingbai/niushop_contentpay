<?php
/**
 * New.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksservice;
use data\service\BaseService;
use data\worksmodel\NvGoodsModel;
use data\worksapi\IGoods;
use think\Log;

class Goods extends BaseService implements IGoods
{
    
    /**
     * 
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     * @return multitype:number unknown
     */
    public function getGoodsList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*'){
        $goods_model = new NvGoodsModel();
        $goods_list = $goods_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        return $goods_list;
    }
    /**
     * 添加修改商品
     * @param unknown $goods_data
     */
    public function addUpdateGoods($goods_data){
        $goods_model = new NvGoodsModel();
        $goods_model->startTrans();
        try {
            $goods_id = $goods_data['goods_id'];
            if(empty($goods_id)){
        
                $goods_data['create_time'] = time();
                $goods_id = $goods_model->save($goods_data);
            }else{
                $goods_data['modify_time'] = time();
                $res = $goods_model->save($goods_data, ['goods_id' => $goods_id]);
            }
            $goods_model->commit();
            return $goods_id;
        
        } catch (\Exception $e) {
            $goods_model->rollback();
            return $e->getMessage();
        }
    }
    
    /**
     * 删除商品
     * @param unknown $goods_ids
     * @param string $condition
     */
    public function delGoods($goods_ids, $condition = ''){
        if(!empty($goods_ids)) $condition['goods_id'] = array('in', $goods_ids);
        
        $goods_model = new NvGoodsModel();
        $res = $goods_model->destroy($condition);
        return $res;
    }
    
    
    
    public function getGoodsInfo($goods_id, $condition = '', $field = '*'){
        $goods_model = new NvGoodsModel();
        
        if(!empty($goods_id)) $condition['goods_id'] = $goods_id;
        if(empty($condition)) return false;
        
        $goods_info = $goods_model->getInfo($condition, $field);
        return $goods_info;
    }
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::addUpdateSubject()
     */
    public function addUpdateSubject($subject_data){
        
        $subject_model = new NvSubjectModel();
        $subject_model->startTrans();
        
        try {
            
            $subject_id = $subject_data['subject_id'];
            if(empty($subject_id)){
            
                $data['create_time'] = time();
                $subject_id = $subject_model->save($subject_data);
            }else{
            
                $res = $subject_model->save($subject_data, ['subject_id' => $subject_id]);
            }
            
            //首页专业板块添加修改
            $website_service = new Website();
            $website_service->addUpdateSubjectHomeBlock(['subject_id'=>$subject_id,'subject_name'=> $subject_data['subject_name'], 'parent_subject_id'=> $subject_data['parent_id']]);
            
            $subject_model->commit();
            return $subject_id;
            
        } catch (\Exception $e) {
            
            $subject_model->rollback();
            return $e->getMessage();
        }
        
    }
    
   
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::getSubjectInfo()
     */
    public function getSubjectInfo($subject_id, $condition = '', $field = '*'){
        
        $subject_model = new NvSubjectModel();
        
        if(!empty($subject_id)) $condition['subject_id'] = $subject_id;
        if(empty($condition)) return false;
        
        $subject_info = $subject_model->getInfo($condition, $field);
        return $subject_info;
    }
    
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::delSubject()
     */
    public function delSubject($subject_id){
        
        $subject_model = new NvSubjectModel();
        $subject_model->startTrans();
        
        try {
            
            $res = $subject_model->destroy(['subject_id' => $subject_id]);
            
            //如果有该专业有下级也将删除
            $children_subject_list = $subject_model->getQuery(['parent_id' => $subject_id], 'subject_id,parent_id', 'sort asc');
            foreach ($children_subject_list as $item){
                $this->delSubject($item['subject_id']);
            }
            
            //删除专业首页显示
            $website_service = new Website();
            $website_service->delSubjectHomeBlock($subject_id);
            
            $subject_model->commit();
            return 1;
        } catch (\Exception $e) {
            
            Log::write('删除专业分类失败，错误：'. $e->getMessage());
            $subject_model->rollback();
            return $e->getMessage();
        }
      
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::getSubjectList()
     */
    public function getSubjectList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*'){
        
       
        
        $subject_model = new NvSubjectModel();
        $subject_list = $subject_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        
        
        
        return $subject_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::addUpdateTeacher()
     */
    public function addUpdateTeacher($teacher_data){
        
        //获取教师详情
//         $teacher_info = $this->getTeacherInfo($uid);
        
        $teacher_model = new NvTeacherModel();
        $teacher_data['modify_time'] = time();
        
        $teacher_id = $teacher_data['id'];

        $res = '';
        
        if(empty($teacher_id)){
        
            $teacher_data['teacher_no'] = 'JS'.time();
            $teacher_data['create_time'] = time();
            $res = $teacher_model->save($teacher_data);
        }else{
            
            $res = $teacher_model->save($teacher_data, ['id'=>$teacher_id]);
        }
        
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::delTeacher()
     */
    public function delTeacher($ids, $condition = ''){
        
        if(!empty($ids)) $condition['id'] = array('in', $ids);
        
        $teacher_model = new NvTeacherModel();
        $res = $teacher_model->destroy($condition);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::getTeacherInfo()
     */
    public function getTeacherInfo($id, $condition = '', $field = '*'){
        
        if(!empty($id)) $condition['id'] = $id;
        
        $teacher_model = new NvTeacherModel();
        $teacher_info = $teacher_model->getInfo($condition, $field);
        return $teacher_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::getTeacherList()
     */
    public function getTeacherList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*'){
        
        $teacher_model = new NvTeacherModel();
        $teacher_list = $teacher_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        
        foreach ($teacher_list['data'] as $item){
            
            if($item['is_start']  == 1){
                $item['start_name'] = '高级讲师';
            }else{
                $item['start_name'] = '首席讲师';
            }
            
            $item['subject_name'] = $this->getSubjectInfo($item['subject_id'])['subject_name'];
            $item['create_time'] = getTimeStampTurnTime($item['create_time']);
            $item['modify_time'] = getTimeStampTurnTime($item['modify_time']);
        }
        return $teacher_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::addUpdateCourse()
     */
    public function addUpdateCourse($course_data, $teacher_ids = ''){
        
        $subject_id = $course_data['subject_id'];
        unset($course_data['subject_id']);
        
        $subject_id_1 = $this->getSubjectInfo(0,['subject_id'=>$subject_id],'parent_id')['parent_id'];
        if(empty($subject_id_1)){
            
            $course_data['subject_id_1'] = $subject_id;
            $course_data['subject_id_2'] = 0;
        }else{
            $course_data['subject_id_1'] = $subject_id_1;
            $course_data['subject_id_2'] = $subject_id;
        }
        
        $course_model = new NvCourseModel();
        
        $course_model->startTrans();
        
        try {
            
            $course_data['end_time'] = empty($course_data['end_time']) ? 0 :getTimeTurnTimeStamp($course_data['end_time']);
            $course_data['release_uid'] = $this->uid;
            $res = '';
            
            $course_id = $course_data['course_id'];
            if(empty($course_id)){
            
                $course_data['create_time'] = time();
                $res = $course_model->save($course_data);
                $course_id = $res;
            }else{
            
                $course_data['modify_time'] = time();
                $res = $course_model->save($course_data, ['course_id' => $course_id]);
            }
   
            $this->addCourseTeacher($course_id, $teacher_ids);
   
            $course_model->commit();
            
            return 1;
        } catch (\Exception $e) {
            
            $course_model->rollback();
            return $e->getMessage();
        }

        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\ICourse::updateCourseSort()
     */
    public function updateCourseSort($course_id, $sort){
    
        $course_model = new NvCourseModel();
        $res = $course_model->save(['sort' => $sort], ['course_id'=> $course_id]);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::delCourse()
     */
    public function delCourse($course_ids, $condition = ''){
        
        if(!empty($course_ids)) $condition['course_id'] = array('in', $course_ids);
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $course_model = new NvCourseModel();
        $res = $course_model->destroy($condition);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::getCourseInfo()
     */
    public function getCourseInfo($course_id, $condition = '', $field = '*'){
        
        if(!empty($course_id)) $condition['course_id'] = $course_id;
        if(empty($condition)) return false;
        
        $course_model = new NvCourseModel();
        $course_info = $course_model->getInfo($condition, $field);
        if($field == '*'){
            $course_info['end_date_time'] = getTimeStampTurnTime($course_info['end_time'], 'Y-m-d');
            $course_info['course_teacher_list'] = $this->getCourseTeacher($course_id);
        }
        
        return $course_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::getCourseList()
     */
    public function getCourseList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*'){
        
        $course_service = new NvCourseModel();
        $course_list = $course_service->pageQuery($page_index, $page_size, $condition, $order, $field);
        
        foreach ($course_list['data'] as $item){
            
            $status_name = '草稿';
            switch ($item['status']){
                case 1:
                    $status_name = '更新中';
                    break;
                case 2:
                    $status_name = '完成';
                    break;
                default:break;
            }
            $item['status_name'] = $status_name;
            
            if(!empty($item['subject_id_2'])){
                
                $item['subject_name'] = $this->getSubjectInfo($item['subject_id_2'],'','subject_name')['subject_name'];
            }else{
                $item['subject_name'] = $this->getSubjectInfo($item['subject_id_1'],'','subject_name')['subject_name'];
            }
            
            
            if($item['lose_type'] == 1){
                $item['end_time'] = '购买后'. $item['lose_time']. '天';
            }else{
                $item['end_time'] = getTimeStampTurnTime($item['end_time'], 'Y-m-d');
            }
            
            $item['create_time'] = getTimeStampTurnTime($item['create_time']);
            $item['modify_time'] = getTimeStampTurnTime($item['modify_time']);
            $course_teacher_list = $this->getCourseTeacher($item['course_id']);
            $teacher_names = '';
            foreach ($course_teacher_list as $teacher_item){
                $teacher_name = $this->getTeacherInfo($teacher_item['teacher_id'],'','name')['name'];
                $teacher_names .= $teacher_name .'、';
            }
            $item['teacher_names'] = rtrim($teacher_names,'、');
        }
        return $course_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::addUpdateCourseKpoint()
     */
    public function addUpdateCourseKpoint($course_kpoint_data){
        
        $course_kpoint_model = new NvCourseKpointModel();
        $course_kpoint_model->startTrans();
        
        try {
            
            //获取视频时长
            $works_service = new Works();
            $course_kpoint_data['time_length'] = $works_service->getDbWorksInfo(0, ['works_code' => $course_kpoint_data['works_code']], 'time_length')['time_length'];
            
            $res = '';
            $kpoint_id = $course_kpoint_data['kpoint_id'];
            if(empty($kpoint_id)){
            
                $course_kpoint_data['add_time'] = time();
            
                $res = $course_kpoint_model->save($course_kpoint_data);
            }else{
            
                $res = $course_kpoint_model->save($course_kpoint_data, ['kpoint_id'=> $kpoint_id]);
            }
            
            $this->setCourseSumKpoint($course_kpoint_data['course_id']);
            
            $course_kpoint_model->commit();
            return 1;
        } catch (\Exception $e) {
            
            Log::write('添加修改课程目录出错， 错误：'. $e->getMessage());
            $course_kpoint_model->rollback();
            return $e->getMessage();
        }
       
        return $res;
    }
    
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\ICourse::setCourseSumKpoint()
     */
    public function setCourseSumKpoint($course_id){
        
        $course_kpoint_model = new NvCourseKpointModel();
        //获取包含目录的总时长
        $sum_time_length = $course_kpoint_model->getSum(['course_id' => $course_id], 'time_length');
        
        $kpoint_count = $course_kpoint_model->getCount(['parent_id'=> array('neq',0),'course_id' => $course_id]);
        //修改课程的总时长
        $course_model = new NvCourseModel();
        $res = $course_model->save(['time_length' => $sum_time_length, 'lession_num'=> $kpoint_count], ['course_id' => $course_id]);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::delCourseKpoint()
     */
    public function delCourseKpoint($kpoint_id, $condition = ''){
     
        $course_kpoint_model = new NvCourseKpointModel();
        $res = $course_kpoint_model->destroy($kpoint_id);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::getCourseKpointInfo()
     */
    public function getCourseKpointInfo($kpoint_id, $condition = '', $field = '*'){
        
        if(!empty($kpoint_id)) $condition['kpoint_id'] = $kpoint_id;
        if(empty($condition)) return false;
        
        $course_kpoint_model = new NvCourseKpointModel();
        $course_kpoint_info = $course_kpoint_model->getInfo($condition, $field);
        return $course_kpoint_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\ICourse::getCourseKpointList()
     */
    public function getCourseKpointList($course_id, $condition = '', $field = '*', $order = 'sort asc'){
        
        if(!empty($course_id)) $condition['course_id'] = $course_id;
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $condition['parent_id'] = 0;
        $course_kpoint_model = new NvCourseKpointModel();
        $course_kpoint_list = $course_kpoint_model->getQuery($condition, $field, $order);
        
        foreach ($course_kpoint_list as $item){
            
             $children_list = $course_kpoint_model->getQuery(['parent_id' => $item['kpoint_id']], $field, $order);
            
             
             foreach($children_list as $children_item){
                 
                 $vmember_account = new VmemberAccount();
                 $vmember_study = $vmember_account->getVmemberStudyHistoryInfo($children_item['kpoint_id'],'is_study_finish');
                 //学习状态获取 如果 0为学习 1已学习 2已学完
                 if(!empty($vmember_study)){
                     if($vmember_study['is_study_finish'] == 1){
                         
                         $children_item['is_study_status'] = 2;
                     }else{
                         $children_item['is_study_status'] = 1;
                     }
                     
                 }else{
                     
                     $children_item['is_study_status'] = 0;
                 }
                 $children_item['works_url'] = $this->getWorksUrl($children_item['kpoint_id']);
             }
            
             $item['children_list'] = $children_list;
             
        }
        return $course_kpoint_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\ICourse::addCourseTeacher()
     */
    public function addCourseTeacher($course_id, $teacher_ids){
        

        $this->delCourseTeacher($course_id);
        $teacher_id_arr = explode(',', $teacher_ids);
        
        foreach ($teacher_id_arr as $item){
        
            $course_teacher_model = new NvCourseTeacherModel();
            $data = array(
                'course_id' => $course_id,
                'teacher_id'=> $item
            );
            $res = $course_teacher_model->save($data);
        }
         
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\ICourse::delCourseTeacher()
     */
    public function delCourseTeacher($course_id, $condition = ''){
        
        if(!empty($course_id)) $condition['course_id'] = $course_id;
        if(empty($condition)) return false;
        
        $course_teacher_model = new NvCourseTeacherModel();
        $res = $course_teacher_model->where($condition)->delete();
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\ICourse::getCourseTeacher()
     */
    public function getCourseTeacher($course_id, $condition = ''){
        
        if(!empty($course_id)) $condition['course_id'] = $course_id;
        
        $course_teacher_model = new NvCourseTeacherModel();
        $course_teacher_list = $course_teacher_model->getQuery($condition, '*', '');
        
        foreach($course_teacher_list as $item){
            
            $teacher_info = $this->getTeacherInfo($item['teacher_id']);
            $item['teacher_name'] = $teacher_info['name'];
        }
        return $course_teacher_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\ICourse::getTeacherByCourse()
     */
    public function getTeacherByCourse($teacher_id){
        
        
        $course_teacher_model = new NvCourseTeacherModel();
        $course_teacher_list = $course_teacher_model->getQuery(['teacher_id'=>$teacher_id], '*', '');
        
        foreach($course_teacher_list as $item){
        
            $course_info = $this->getCourseInfo($item['course_id']);
            $item['course_info'] = $course_info;
            
        }
        return $course_teacher_list;
    }
    
    /**
     * 传入课程编号获取价格
     * @param unknown $course_list
     */
    public function getCourseListSumMoney($course_list){
        
        $sum_money = 0;
        $course_array = explode(',', $course_list);
        
        foreach($course_array as $item){
            
            $course_model = new NvCourseModel();
            $item_money = $course_model->getSum(['course_id'=>$item], 'current_price')['current_price'];
            $sum_money = $sum_money + $item_money;
        }
        
        return $sum_money;
    }
    
    /**
     * 根据课程编号获取总赠送积分
     * @param unknown $course_list
     */
    public function getCourseListSumGivePoint($course_list){
        
        $sum_give_point = 0;
        $course_array = explode(',', $course_list);
        
        foreach($course_array as $item){
        
            $course_model = new NvCourseModel();
            $item_give_point = $course_model->getSum(['course_id'=>$item], 'give_point')['give_point'];
            $sum_give_point = $sum_give_point + $item_give_point;
        }
        
        return $sum_give_point;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\ICourse::addUpdatePackage()
     */
    public function addUpdatePackage($package_data){
        
        $package_model = new NvPackageModel();
        
        $package_data['modify_time'] = time();
        $package_data['release_uid'] = $this->uid;
        
        if(!empty($package_data['end_time'])) $package_data['end_time'] = getTimeTurnTimeStamp($package_data['end_time']);
        
        $package_id = $package_data['package_id'];
        
        if(empty($package_id)){
            
            $package_data['create_time'] = time();
            
            $package_id = $package_model->save($package_data);
        }else{
         
            $package_model->save($package_data, ['package_id' => $package_id]);
        }
        
        return $package_id;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\ICourse::getPackageInfo()
     */
    public function getPackageInfo($package_id, $condition = '', $field = '*'){
        
        if(!empty($package_id)) $condition['package_id'] = $package_id;
        if(empty($condition)) return false;
        
        $package_model = new NvPackageModel();
        $package_info = $package_model->getInfo($condition, $field);
        
        if(!empty($package_info['end_time'])) $package_info['end_time'] = getTimeStampTurnTime($package_info['end_time'], 'Y-m-d');

        return $package_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\ICourse::getPackageList()
     */
    public function getPackageList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
        
        $package_model = new NvPackageModel();
        $package_list = $package_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        
        foreach ($package_list['data'] as $item){
        
            $status_name = '草稿';
            switch ($item['status']){
                case 1:
                    $status_name = '正常';
                    break;
                default:break;
            }
            $item['status_name'] = $status_name;
        
            if($item['lose_type'] == 1){
                $item['end_time'] = '购买后'. $item['lose_time']. '天';
            }else{
                $item['end_time'] = getTimeStampTurnTime($item['end_time'], 'Y-m-d');
            }
        
            $item['create_time'] = getTimeStampTurnTime($item['create_time']);
            $item['modify_time'] = getTimeStampTurnTime($item['modify_time']);
        }
        return $package_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\ICourse::delPackage()
     */
    public function delPackage($package_ids, $condition = ''){
        
        if(!empty($package_ids)) $condition['package_id'] = array('in', $package_ids);
        if(empty($condition)) return CONDITION_IS_NULL;
        
        $package_model = new NvPackageModel();
        $res = $package_model->destroy($condition);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\ICourse::addUpdatePackageCourse()
     */
    public function addUpdatePackageCourse($data){
    
        $package_course_model = new NvPackageCourseModel();
        $pc_id = $data['pc_id'];
        
        if(empty($pc_id)){
        
            $data['create_time'] = time();
            $package_course_model->save($data);
        }else{
            
            $package_course_model->save($data, ['pc_id' => $pc_id]);
        }
        return 1;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\ICourse::delPackageCourse()
     */
    public function delPackageCourse($pc_id, $condition = ''){
        
        if(!empty($pc_id)) $condition['pc_id'] = $pc_id;
        if(empty($condition)) return false;
        $package_course_model = new NvPackageCourseModel();
        $res = $package_course_model->destroy($condition);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\ICourse::getPackageCourseList()
     */
    public function getPackageCourseList($package_id, $condition = ''){
        
        if(!empty($package_id)) $condition['package_id'] = $package_id;
        if(empty($condition)) return false;
        
        $package_course_model = new NvPackageCourseModel();
        $package_course_list = $package_course_model->getQuery($condition, '*', '');
        
        foreach ($package_course_list as $item){
        
            //添加包含的课程信息
            $course_info = $this->getCourseInfo($item['course_id']);
            $item['course_info'] = $course_info;
            $item['course_info']['create_time'] = getTimeStampTurnTime($course_info['create_time']);
        }
        return $package_course_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\ICourse::addBatchPackageCourse()
     */
    public function addBatchPackageCourse($package_id, $course_ids){
       
        $course_id_array = explode(',', $course_ids);
        foreach($course_id_array as $item){
            $data = array(
                'package_id'    => $package_id,
                'course_id'     => $item
            );
            $this->addUpdatePackageCourse($data);
        }
        return 1;
    }
    
    /**
     * 根据目录id获取视频播放地址
     * @param unknown $kpoint_id
     */
    public function getWorksUrl($kpoint_id){
    
        $course_service = new Course();
        $course_info = $course_service->getCourseKpointInfo($kpoint_id,'','course_id,works_code');
    
        $uid = $this->uid;
        $condition = array(
            'relation_type' => 1,
            'relation_id'   => $course_info['course_id'],
            'uid'           => $uid,
            'end_time'      => array('lt', time())
        );
    
        $v_member_service = new Vmember();
        $member_have = $v_member_service->getVmemberHaveInfo(0, $condition);
//         if(empty($member_have)){
    
//             //该会员没有购买该课程
//             return array('code'=>NV_MEMBER_NO_COURSE);
//         }
    
        $works_service = new Works();
        $works_info = $works_service->getDbWorksInfo(0, ['works_code'=> $course_info['works_code']],'works_url');
        return array('code'=>1,'works_url'=>$works_info['works_url']);
    }
}
